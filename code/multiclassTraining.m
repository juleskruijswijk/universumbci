run ../../utilities/initPaths.m;

% N.B. only really need the header to get the channel information, and sample rate
buffhost='localhost';buffport=1972;
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% Constants
capFile='cap_tmsi_mobita_im.txt';
overridechnm=1; % capFile channel names override those from the header!
nSubject = 1; 
dname  = ['training_data_' num2str(nSubject)];
cname_vert = ['mk_clsfr_vert_' num2str(nSubject)];
cname_hori = ['mk_clsfr_hori_' num2str(nSubject)];


% useful functions
load(dname);
data = sdata;
devents = sdevents;
% Re-label 
% positive class:    1:= all ups for vert, all rights for hori
% negative class:   -1:= all downs for vert, all lefts for hori
% ignored class:     0
for ei=1:numel(devents)
    switch devents(ei).label
        case 'up'
            vert_devents(ei).label =  1;
            hori_devents(ei).label =  0;
        case 'left'
            vert_devents(ei).label =  0;
            hori_devents(ei).label = -1;
        case 'right'
            vert_devents(ei).label =  0;
            hori_devents(ei).label =  1;
        case 'down'
            vert_devents(ei).label = -1;
            hori_devents(ei).label =  0;
        case 'upleft'
            vert_devents(ei).label =  1;
            hori_devents(ei).label =  -1;
        case 'upright'
            vert_devents(ei).label =  1;
            hori_devents(ei).label =  1;
        case 'downleft'
            vert_devents(ei).label = -1;
            hori_devents(ei).label = -1;
        case 'downright'
            vert_devents(ei).label = -1;
            hori_devents(ei).label =  1;
    end
end

% train classifier
clsfr_vert = buffer_train_ersp_clsfr(vert_data,vert_devents,hdr,'spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnm',overridechnm);
clsfr_hori = buffer_train_ersp_clsfr(hori_data,hori_devents,hdr,'spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnm',overridechnm);
fprintf('Saving classifier to : %s\n',cname_vert);
save(cname_vert,'-struct','clsfr_vert');
fprintf('Saving classifier to : %s\n',cname_hori);
save(cname_hori,'-struct','clsfr_hori');
sendEvent('phase.training','start');