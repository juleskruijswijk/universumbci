% Analysis

capFile='cap_tmsi_mobita_im.txt';
overridechnm=1; % capFile channel names override those from the header!
nSubject = 1; 
nFold = 5; % possible 2,5,6,10
dname  = ['training_data_' num2str(nSubject)];
dname_1 = ['uni_vert_devents' num2str(nSubject)];
dname_2 = ['uni_hori_devents' num2str(nSubject)];
dname_3 = ['mk_vert_devents' num2str(nSubject)];
dname_4 = ['mk_hori_devents' num2str(nSubject)];

load(dname);
load(dname_1);
load(dname_2);
load(dname_3);
load(dname_4);

