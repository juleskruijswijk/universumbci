run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% set the real-time-clock to use
initgetwTime;
initsleepSec;

% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

clsfr = load('clsfr.mat');
trlen_ms = 600;

exprun = true;
state = [];
pred = 0;

while exprun
    seqrun = true;
    pred(:) = 0;
    while exprun && seqrun
        [data,devents,state]=buffer_waitData(buffhost,buffport,[],'startSet',...
            {{'feedStim.flashRow' 'feedStim.flashCol'}},'trlen_ms',trlen_ms,...
            'exitSet',{{'feedSeq.End' 'feedExp.End'}});
        if ~isempty(devents)
            for i=1:numel(devents)
                if matchEvents(devents(i), 'feedSeq.End', true)
                    seqrun = false;
                elseif matchEvents(devents(i), 'feedExp.End', true);
                    exprun = falsE;
                elseif matchEvents(devents(i), {'feedStim.flashRow', 'feedStim.flashCol'})
                    [f,fraw,p]=buffer_apply_erp_clsfr(data(i).buf,clsfr);
                    if matchEvents(devents(i), 'feedStim.flashRow')
                        pred(1,devents(i).value) = f;
                    elseif matchEvents(devents(i), 'feedStim.flashCol')
                        pred(2,devents(i).value) = f;
                    end
                end
            end
        end
    end
    if ~seqrun
        sendEvent('classifier.pred', pred(:));
    end
end

% useful functions
% apply classification pipeline to this events data
% [f,fraw,p]=buffer_apply_erp_clsfr(data.buf,clsfr);
