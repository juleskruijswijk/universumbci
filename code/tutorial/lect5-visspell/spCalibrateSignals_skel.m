run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% set the real-time-clock to use
initgetwTime;
initsleepSec;

trlen_ms=600;
% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

[data,devents,state]=buffer_waitData(buffhost,buffport,[],'startSet',...
        {'calStim.Onset'},'trlen_ms',trlen_ms,'exitSet',{'calStim.EndExp'})
me = matchEvents(devents,'calStim.EndExp','End');
devents(me)=[];
data(me)=[];
save('Blub', 'data', 'devents','hdr');
% useful functions
% buffer_waitData(???)