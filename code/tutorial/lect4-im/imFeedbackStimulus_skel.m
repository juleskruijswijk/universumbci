run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% set the real-time-clock to use
initgetwTime;
initsleepSec;

% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------
verb=0;
nSymbs=2;
nSeq=5;
nBlock=5;%10; % number of stim blocks to use
trialDuration=3;
baselineDuration=1;
intertrialDuration=2;
moveScale = .1; % used to transform between predictions and size of move on screen



% make the stimulus
clf;
set(gcf,'color',[0 0 0],'toolbar','none','menubar','none'); % black figure
set(gca,'visible','off','color',[0 0 0]); % black axes
h=text(.5,.5,'text','HorizontalAlignment','center','VerticalAlignment','middle',...
       'FontUnits','normalized','fontsize',.2,'color',[1 1 1],'visible','on');
startString = {'Press any Button,', 'when you are', 'ready to start!'};
contString = {'Press any Button,', 'when you are', 'ready to continue!'};
set(h,'string',startString);
set(h,'fontsize',.14);
drawnow;
w = waitforbuttonpress;
if w==1
    % send event annotating the current time
    sendEvent('stimulus.imStart','startExperiment');
end

%tgtSeq=mkStimSeqRand(nBlock,nSeq);

state = [];


for i=1:nBlock
    sendEvent('stimulus.imEpoch',strcat('startEpoch ',int2str(i)));
    for j=1:nSeq
        set(h,'string','+');
        set(h,'color',[1 1 1]);  
        set(h,'fontsize',.3);
        drawnow;
        sendEvent('stimulus.imBase','showBaseline');
        sleepSec(baselineDuration);
        set(h,'visible','off');
        drawnow;
        [devents,state]=buffer_newevents(buffhost,buffport,state,{'classifier.imPred'},[],500);
        if ~isempty(devents)
            if devents.value > 0
                set(h,'string','L');
                set(h,'visible','on');
                drawnow;
                sendEvent('classifier.imResp','L');
            elseif devents.value < 0
                set(h,'string','R');
                set(h,'visible','on');
                drawnow;
                sendEvent('classifier.imResp','R');
            end
        else
            set(h,'string','No f****** clue!');  
            set(h,'fontsize',.14);
            set(h,'visible','on');
            drawnow;
            sendEvent('classifier.imResp','noclue');
        end
        sleepSec(trialDuration);
        set(h,'string','+');
        set(h,'color',[0 0 1]);
        drawnow;
        sendEvent('stimulus.imCueEnd','cueEnd');
        sleepSec(intertrialDuration);
    end
    if i~=nBlock
        set(h,'string',contString);
        set(h,'color',[1 1 1]);  
        set(h,'fontsize',.14);
        drawnow;
        w = waitforbuttonpress;
    end
end
set(h,'string','Thank you!');
set(h,'color',[1 1 1]);  
set(h,'fontsize',.14);
drawnow;
sendEvent('stimulus.imEndExp','endExperiment');
