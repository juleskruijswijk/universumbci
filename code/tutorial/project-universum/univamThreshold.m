run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% Name for classifier file
validSubj = false;
nSubj = 0;
while ~validSubj
    prompt = {'Enter Subject Number:'};
    dlg_title = 'Input';
    num_lines = 1;
    def = {'0'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    
    fileNameClsfrUNIv = ['data/subject_' num2str(answer{1,1}) '/universum/clsfr_vert' ];
    fileNameClsfrUNIh = ['data/subject_' num2str(answer{1,1}) '/universum/clsfr_hori' ];
    fileNameClsfrMC8v = ['data/subject_' num2str(answer{1,1}) '/multi_class_8/clsfr_vert' ];
    fileNameClsfrMC8h = ['data/subject_' num2str(answer{1,1}) '/multi_class_8/clsfr_hori' ];
    if exist([fileNameClsfrUNIv '.mat'], 'file') && exist([fileNameClsfrUNIh '.mat'], 'file') ...
            && exist([fileNameClsfrMC8v '.mat'], 'file') && exist([fileNameClsfrMC8h '.mat'], 'file') 
        validSubj = true;
        nSubj = answer{1,1};
    else
        uiwait(msgbox('No(/Not all) classifiers for this subject available!', 'Error','error'));
    end
end

clsfrUNIv = load(fileNameClsfrUNIv);
clsfrUNIh = load(fileNameClsfrUNIh);
clsfrMC8v = load(fileNameClsfrMC8v);
clsfrMC8h = load(fileNameClsfrMC8h);

% Name for training file
fileNameTD = ['data/subject_' num2str(nSubj) '/training_data'];

load(fileNameTD);

hDP = numel(sdevents)/8;

% Name for save-files
% Graphs
folderPlots = ['data/subject_' num2str(nSubj) '/plots'];

% Threshold
fileNameThresUNI = ['data/subject_' num2str(nSubj) '/universum/thres' ];
fileNameThresMC8 = ['data/subject_' num2str(nSubj) '/multi_class_8/thres' ];

% Separation of data 
llData = sdata(find(cellfun( @(x) strcmp(x,'left'), {sdevents.value})));
rrData = sdata(find(cellfun( @(x) strcmp(x,'right'), {sdevents.value})));
uuData = sdata(find(cellfun( @(x) strcmp(x,'up'), {sdevents.value})));
ddData = sdata(find(cellfun( @(x) strcmp(x,'down'), {sdevents.value})));

% Vertical
predUNIv = zeros(4*hDP,2);
predUNIv(1:hDP,1) = 1;
predUNIv(hDP+1:3*hDP,1) = 0;
predUNIv(3*hDP+1:4*hDP,1) = -1;
predMC8v = zeros(4*hDP,2);
predMC8v(1:4*hDP,1) = 1;
predMC8v(hDP+1:3*hDP,1) = 0;
predMC8v(3*hDP+1:4*hDP,1) = -1;
for i=1:hDP
    [f_v,~,~]=buffer_apply_ersp_clsfr(uuData(i).buf,clsfrUNIv);
    predUNIv(i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(llData(i).buf,clsfrUNIv);
    predUNIv(hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(rrData(i).buf,clsfrUNIv);
    predUNIv(2*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(ddData(i).buf,clsfrUNIv);
    predUNIv(3*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(uuData(i).buf,clsfrMC8v);
    predMC8v(i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(llData(i).buf,clsfrMC8v);
    predMC8v(hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(rrData(i).buf,clsfrMC8v);
    predMC8v(2*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(ddData(i).buf,clsfrMC8v);
    predMC8v(3*hDP+i,2) = f_v;
end

% Horizontal
predUNIh = zeros(4*hDP,2);
predUNIh(1:hDP,1) = 1;
predUNIh(hDP+1:3*hDP,1) = 0;
predUNIh(3*hDP+1:4*hDP,1) = -1;
predMC8h = zeros(4*hDP,2);
predMC8h(1:hDP,1) = 1;
predMC8h(hDP+1:3*hDP,1) = 0;
predMC8h(3*hDP+1:4*hDP,1) = -1;
for i=1:hDP
    [f_v,~,~]=buffer_apply_ersp_clsfr(rrData(i).buf,clsfrUNIh);
    predUNIh(i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(uuData(i).buf,clsfrUNIh);
    predUNIh(hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(ddData(i).buf,clsfrUNIh);
    predUNIh(2*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(llData(i).buf,clsfrUNIh);
    predUNIh(3*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(rrData(i).buf,clsfrMC8h);
    predMC8h(i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(uuData(i).buf,clsfrMC8h);
    predMC8h(hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(ddData(i).buf,clsfrMC8h);
    predMC8h(2*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(llData(i).buf,clsfrMC8h);
    predMC8h(3*hDP+i,2) = f_v;
end

% Universum Vertical
nBins = 60;
bound = max(abs(predUNIv(:,2)));
figure(1);
bins = linspace(-bound, bound, nBins);
h1 = hist(predUNIv(1:hDP,2),bins); 
h2 = hist(predUNIv(hDP+1:3*hDP,2),bins); 
h3 = hist(predUNIv(3*hDP+1:4*hDP,2),bins);
bar([h1', h2', h3']);
title('Universum Vertical');
xlabel('Classification Decision Value');
ylabel('Number Of Events');
legend('Up','Horizontal','Down');
xd = findobj('-property','XData');

for i=1:2
    dat = get(xd(i),'XData');
    dat2 = (2*bound)*dat/nBins - bound;
    set(xd(i),'XData',dat2);
end
print(gcf, [folderPlots '/hist_predUNIv.eps'], '-depsc2');

% Universum Horizontal
bound = max(abs(predUNIh(:,2)));
figure(2);
bins = linspace(-bound, bound, nBins);
h1 = hist(predUNIh(1:hDP,2),bins);
h2 = hist(predUNIh(hDP+1:3*hDP,2),bins);
h3 = hist(predUNIh(3*hDP+1:4*hDP,2),bins);
bar([h1', h2', h3']);
title('Universum Horizontal');
xlabel('Classification Decision Value');
ylabel('Number Of Events');
legend('Right', 'Vertical', 'Left');
xd = findobj('-property','XData');

for i=1:2
    dat = get(xd(i),'XData');
    dat2 = (2*bound)*dat/nBins - bound;
    set(xd(i),'XData',dat2);
end
print(gcf, [folderPlots '/hist_predUNIh.eps'], '-depsc2');
% MultiClass Vertical
bound = max(abs(predMC8v(:,2)));
figure(3);
bins = linspace(-bound, bound, nBins);
h1 = hist(predMC8v(1:hDP,2),bins);
h2 = hist(predMC8v(hDP+1:3*hDP,2),bins);
h3 = hist(predMC8v(3*hDP+1:4*hDP,2),bins);
bar([h1', h2', h3']);
title('MultiClass Vertical');
xlabel('Classification Decision Value');
ylabel('Number Of Events');
legend('Up','Horizontal','Down');
xd = findobj('-property','XData');

for i=1:2
    dat = get(xd(i),'XData');
    dat2 = (2*bound)*dat/nBins - bound;
    set(xd(i),'XData',dat2);
end
print(gcf, [folderPlots '/hist_predMC8v.eps'], '-depsc2');
% MultiClass Horizontal
bound = max(abs(predMC8h(:,2)));
figure(4);
bins = linspace(-bound, bound, nBins);
h1 = hist(predMC8h(1:hDP,2),bins);
h2 = hist(predMC8h(hDP+1:3*hDP,2),bins);
h3 = hist(predMC8h(3*hDP+1:4*hDP,2),bins);
bar([h1', h2', h3']);
title('MultiClass Horizontal');
xlabel('Classification Decision Value');
ylabel('Number Of Events');
legend('Right', 'Vertical', 'Left');
xd = findobj('-property','XData');

for i=1:2
    dat = get(xd(i),'XData');
    dat2 = (2*bound)*dat/nBins - bound;
    set(xd(i),'XData',dat2);
end
print(gcf, [folderPlots '/hist_predMC8h.eps'], '-depsc2');

% Threshold calculation
thresUNI = [0 0; 0 0]; % upper row horizontal
thresMC8 = [0 0; 0 0];

normPredUNIh(1) = fitdist(predUNIh(1:hDP,2),'Normal');
normPredUNIh(2) = fitdist(predUNIh(hDP+1:3*hDP,2),'Normal');
normPredUNIh(3) = fitdist(predUNIh(3*hDP+1:4*hDP,2),'Normal');
[thresUNI(1,1), thresUNI(1,2)]=calcThres(normPredUNIh(1),normPredUNIh(2),normPredUNIh(3));

normPredUNIv(1) = fitdist(predUNIv(1:hDP,2),'Normal');
normPredUNIv(2) = fitdist(predUNIv(hDP+1:3*hDP,2),'Normal');
normPredUNIv(3) = fitdist(predUNIv(3*hDP+1:4*hDP,2),'Normal');
[thresUNI(2,1), thresUNI(2,2)]=calcThres(normPredUNIv(1),normPredUNIv(2),normPredUNIv(3));

normPredMC8h(1) = fitdist(predMC8h(1:hDP,2),'Normal');
normPredMC8h(2) = fitdist(predMC8h(hDP+1:3*hDP,2),'Normal');
normPredMC8h(3) = fitdist(predMC8h(3*hDP+1:4*hDP,2),'Normal');
[thresMC8(1,1), thresMC8(1,2)]=calcThres(normPredMC8h(1),normPredMC8h(2),normPredMC8h(3));

normPredMC8v(1) = fitdist(predMC8v(1:hDP,2),'Normal');
normPredMC8v(2) = fitdist(predMC8v(hDP+1:3*hDP,2),'Normal');
normPredMC8v(3) = fitdist(predMC8v(3*hDP+1:4*hDP,2),'Normal');
[thresMC8(2,1), thresMC8(2,2)]=calcThres(normPredMC8v(1),normPredMC8v(2),normPredMC8v(3));

save(fileNameThresUNI, 'thresUNI');
save(fileNameThresMC8, 'thresMC8');

save([folderPlots '/dist_predUNIv'], 'normPredUNIv');
save([folderPlots '/dist_predUNIh'], 'normPredUNIh');
save([folderPlots '/dist_predMC8v'], 'normPredMC8v');
save([folderPlots '/dist_predMC8h'], 'normPredMC8h');
close all;