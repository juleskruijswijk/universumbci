run ../../utilities/initPaths.m;

% Constants
capFile='1010.txt';
overridechnm=1; % capFile channel names override those from the header!
% IO-Variables
valid_subj = false;
nSubj = 0;
while ~valid_subj
    prompt = {'Enter Subject Number:'};
    dlg_title = 'Input';
    num_lines = 1;
    def = {'0'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    
    fileNameTD = ['data/subject_' num2str(answer{1,1}) '/training_data' ];
    if exist([fileNameTD '.mat'], 'file') 
        valid_subj = true;
        nSubj = answer{1,1};
        folderUNI = ['data/subject_' num2str(answer{1,1}) '/universum'];
        mkdir(folderUNI);
%        folderMC4 = ['data/subject_' num2str(answer{1,1}) '/multi_class_4'];
%        mkdir(folderMC4);
        folderMC8 = ['data/subject_' num2str(answer{1,1}) '/multi_class_8'];
        mkdir(folderMC8);
    else
        uiwait(msgbox('No training data for this subject available!', 'Error','error'));
    end
end
folderSubj = ['data/subject_' num2str(nSubj) '/'];
fileNameTD  = [folderSubj 'training_data'];
fileNameUNIv = [folderSubj '/universum/clsfr_vert'];
fileNameUNIh = [folderSubj '/universum/clsfr_hori'];
% fileNameMC4v = [folderSubj '/multi_class_4/clsfr_vert'];
% fileNameMC4h = [folderSubj '/multi_class_4/clsfr_hori'];
fileNameMC8v = [folderSubj '/multi_class_8/clsfr_vert'];
fileNameMC8h = [folderSubj '/multi_class_8/clsfr_hori'];

% Load Data and generate devents-files for the various classifiers
load(fileNameTD);
nDP = numel(sdevents);
deventsUNIv = sdevents;
deventsUNIh = sdevents;
% deventsMC4v = sdevents;
% deventsMC4h = sdevents;
deventsMC8v = sdevents;
deventsMC8h = sdevents;
% Re-label Universum
% positive class:    1:= up for vert, right for hori
% negative class:   -1:= down for vert, left for hori
% universum class:   2:= left/right for vert, up/down for hori 
%
% ignored class:     0:= all diagonal directions
%
% Re-label Multiclass (4 directions)
% positive class:    1:= up for vert, right for hori
% negative class:   -1:= down for vert, left for hori
%
% ignored class:     0:= left/right for vert, up/down for hori, 
%                           all diagonal directions
%
% Re-label Multiclass (8 directions)
% positive class:    1:= all cases containing up for vert, all cases
%                           containing right for hori
% negative class:   -1:= all cases containing down for vert, all cases
%                           containing left for hori
% ignored class:     0:= pure left/right for vert, pure up/down for hori
for ei=1:numel(sdevents)
    switch sdevents(ei).value
        case 'up'
            % Universum
            deventsUNIv(ei).value =  1;
            deventsUNIh(ei).value =  2;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  1;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  1;
            deventsMC8h(ei).value =  0;
        case 'left'
            % Universum
            deventsUNIv(ei).value =  2;
            deventsUNIh(ei).value =  -1;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value = -1;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  0;
            deventsMC8h(ei).value = -1;
        case 'right' % is back to correct version
            % Universum
            deventsUNIv(ei).value =  2;
            deventsUNIh(ei).value =  1;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  1;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  0;
            deventsMC8h(ei).value =  1;
        case 'down' % is back to correct version
            % Universum
            deventsUNIv(ei).value =  -1;
            deventsUNIh(ei).value =  2;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value = -1;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  -1;
            deventsMC8h(ei).value =  0;
        case 'upleft'
            % Universum
            deventsUNIv(ei).value =  0;
            deventsUNIh(ei).value =  0;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  1;
            deventsMC8h(ei).value = -1;
        case 'downleft'
            % Universum
            deventsUNIv(ei).value =  0;
            deventsUNIh(ei).value =  0;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  -1;
            deventsMC8h(ei).value =  -1;
        case 'downright'
            % Universum
            deventsUNIv(ei).value =  0;
            deventsUNIh(ei).value =  0;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  -1;
            deventsMC8h(ei).value =  1;
        case 'upright'
            % Universum
            deventsUNIv(ei).value =  0;
            deventsUNIh(ei).value =  0;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  1;
            deventsMC8h(ei).value =  1;
    end
end

% train classifier
% universum classifiers  
fprintf('Generating classifier for universum/vertical \n');
clsfrUNIv = buffer_train_ersp_clsfr(sdata,deventsUNIv,hdr,'objFn','universum','spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
movefile('AUC.pdf',[folderSubj '/universum/AUC_uni_vert.pdf']);
movefile('ERSP.pdf',[folderSubj '/universum/ERSP_uni_vert.pdf']);
fprintf('Generating classifier for universum/horizontal \n');
clsfrUNIh = buffer_train_ersp_clsfr(sdata,deventsUNIh,hdr,'objFn','universum','spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
movefile('AUC.pdf',[folderSubj '/universum/AUC_uni_hori.pdf']);
movefile('ERSP.pdf',[folderSubj '/universum/ERSP_uni_hori.pdf']);

% % multiclass classifiers (4 directions)
% fprintf('Generating classifier for 4-multiclass/vertical \n');
% clsfrMC4v = buffer_train_ersp_clsfr(sdata,deventsMC4v,hdr,'spatialfilter','slap','freqband',...
%     [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
% fprintf('Generating classifier for 4-multiclass/horizontal \n');
% clsfrMC4h = buffer_train_ersp_clsfr(sdata,deventsMC4h,hdr,'spatialfilter','slap','freqband',...
%     [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);

% multiclass classifiers (8 directions)
fprintf('Generating classifier for 8-multiclass/vertical \n');
clsfrMC8v = buffer_train_ersp_clsfr(sdata(1:1/2*nDP),deventsMC8v(1:1/2*nDP),hdr,'spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
movefile('AUC.pdf',[folderSubj '/multi_class_8/AUC_mk8_vert.pdf']);
movefile('ERSP.pdf',[folderSubj '/multi_class_8/ERSP_mk8_vert.pdf']);
fprintf('Generating classifier for 8-multiclass/horizontal \n');
clsfrMC8h = buffer_train_ersp_clsfr(sdata(1:1/2*nDP),deventsMC8h(1:1/2*nDP),hdr,'spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
movefile('AUC.pdf',[folderSubj '/multi_class_8/AUC_mk8_hori.pdf']);
movefile('ERSP.pdf',[folderSubj '/multi_class_8/ERSP_mk8_hori.pdf']);
% multiclass classifiers (8 directions)
% fprintf('Generating classifier for 8-multiclass/vertical \n');
% clsfrMC8v = buffer_train_ersp_clsfr(sdata(4*nDP+1:8*nDP),deventsMC8v(4*nDP+1:8*nDP),hdr,'spatialfilter','slap','freqband',...
%     [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
% fprintf('Generating classifier for 8-multiclass/horizontal \n');
% clsfrMC8h = buffer_train_ersp_clsfr(sdata(4*nDP+1:8*nDP),deventsMC8h(4*nDP+1:8*nDP),hdr,'spatialfilter','slap','freqband',...
%     [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
% save classifiers
% universum classifiers
fprintf('Saving classifier to : %s\n',fileNameUNIv);
save(fileNameUNIv,'-struct','clsfrUNIv');
fprintf('Saving classifier to : %s\n',fileNameUNIh);
save(fileNameUNIh,'-struct','clsfrUNIh');
% multiclass classifiers (4 directions)
% fprintf('Saving classifier to : %s\n',fileNameMC4v);
% save(fileNameMC4v,'-struct','clsfrMC4v');
% fprintf('Saving classifier to : %s\n',fileNameMC4h);
% save(fileNameMC4h,'-struct','clsfrMC4h');
% multiclass classifiers (8 directions)
fprintf('Saving classifier to : %s\n',fileNameMC8v);
save(fileNameMC8v,'-struct','clsfrMC8v');
fprintf('Saving classifier to : %s\n',fileNameMC8h);
save(fileNameMC8h,'-struct','clsfrMC8h');