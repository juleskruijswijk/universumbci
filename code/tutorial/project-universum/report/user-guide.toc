\select@language {english}
\contentsline {section}{\numberline {1}User Guide}{2}
\contentsline {subsection}{\numberline {1.1}Introduction}{2}
\contentsline {subsection}{\numberline {1.2}Preparation}{2}
\contentsline {subsection}{\numberline {1.3}Training}{2}
\contentsline {subsection}{\numberline {1.4}Classifier Training}{3}
\contentsline {subsection}{\numberline {1.5}Testing}{3}
\contentsline {section}{\numberline {2}Programmer Guide}{5}
\contentsline {subsection}{\numberline {2.1}Introduction}{5}
\contentsline {subsection}{\numberline {2.2}Training}{5}
\contentsline {subsection}{\numberline {2.3}Test}{6}
\contentsline {subsection}{\numberline {2.4}Matlab-Files}{8}
\contentsline {subsubsection}{\numberline {2.4.1}State variables and Containers}{10}
\contentsline {subsubsection}{\numberline {2.4.2}Events}{12}
