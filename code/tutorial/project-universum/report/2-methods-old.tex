\section{Methods}

\subsection{Design}
In order to provide an answer to the proposed research question, a BCI system has been designed.
This means that via ways of training a participant on certain behaviour will enhance the test results and is needed to train the classifiers.
Therefore, before any results about testing will be gathered, the subject will be trained with stimuli that stimulate activity in the brain regions of interest.
The data from the training phase can be used to train the classifiers, which also gives the ability to measure performance between subjects and between the multi-class and the universum classifier.
In the test phase another quantitative score will be measured, which is based on a time cost.

\subsection{Participants}
Four right handed, healthy participants, with no history of epilepsy or seizure were included in the study. All were students of Radboud University and co-authors of the present study. Subject 1 (male, age 26 years) performed session 1. Subject 2 (male, age 30) performed sessions 2, 5, and 8. Subject 3 (female, age 22) performed sessions 3, 4, 6, and 9. Subject 4 (male, age 22) performed session 7.

\subsection{Materials}
To control the BCI environment the BCI toolbox developed by Jason Farquhar has been used.
This toolbox is an essential part of a BCI buffer system in order to exchange information between the different systems that are being used (e.g. the EEG system and the machine that is running the program).
In the case of this experiment, the toolbox has been expanded to allow for the use of both classifiers and a 2D cursor control environment\footnote{See the user's and programmer's guide for detailed information about the expansion of the software}.
To retrieve EEG data, a 64 channel BioSemi EEG system was used.
For stimuli, simple pictures of a tongue, feet, and left and right hands were used in the training phase.
A cursor was used in the test phase.
Participants were facing a monitor in a sound-proof room so no external noise could disturb them.
The experimenters were able to see and hear the participant via a camera and microphone placed in the room.

\subsection{Procedure}
The experimental procedure consisted of several phases. The flowchart below (fig. \ref{tikz:flow}) provides an overview the different parts and the order in which they followed each other. The next subsections will further elaborate on the details of respective phases of the experiment.

\begin{figure}
\begin{center}
\begin{tikzpicture}[auto]
% Place nodes
\node [block] (inst) {Capfitting and instructions (30 minutes)};
\node [block, below of=inst] (warm) {Warm-up phase (1 minute)};
\node [block, below of=warm] (train) {Training phase (30 minutes)};
\node [block, below of=train] (clas) {Classification (5 minutes)};
\node [block, below of=clas] (test) {Test phase (15 minutes)};
% Draw edges
\path [line] (inst) -- (warm);
\path [line] (warm) -- (train);
\path [line] (train) -- (clas);
\path [line] (clas) -- (test);
\end{tikzpicture}
\end{center}
\label{tikz:flow}
\caption{Flowchart for different phases in the experiment. The time shows the total time cost for each phase.}
\end{figure}

\subsection{Training task}
During training, the subjects performed left hand, right hand, feet, and tongue movements as well as combinations thereof according to instructions.
Each epoch started with the presentation of a stimulus (simple picture of a left hand, right hand, feet, tongue, or a combination of two pictures) during $2$ seconds. This was followed by a period where the screen would be black ($3$ seconds). At this point the instruction specified that subject should move the body part or combination of body parts that had been presented. The specific movements to be performed were explained in the instructions preceding the task. This blank screen was followed by a green X presented for 2 seconds, signalling that movement should cease, after which the next epoch started.
This process was repeated for all valid combinations of stimuli ( $8$ in total, namely: left hand, right hand, tongue, feet, left hand-tongue, left hand-feet, right hand-tongue, right hand-feet). In one sequence, each of the $8$ movements was performed once, in randomised order. In total, the training consisted of $30$ of such sequences, thus each movement was performed $30$ times. After every $6$ sequences, the subject was given a break until they were ready to continue which they announced verbally.

\subsection{Data collection -- training}
$2$ seconds of EEG data were collected in $3$ second intervals  during the period the subject was moving (ignoring the first and the last $500$ ms) and labelled according to the body part being moved. 

\subsection{Cursor control test}
The intended application of our BCI was cursor control. Hence, the last part of the experiment consisted of a cursor control paradigm using the classifier trained based on the data from the previously mentioned training task. In the cursor control test a round target was presented on a black screen. The subjects were instructed to move the cursor from the center of the screen to the target using the movements corresponding with the direction of the target. Once the target was reached it would disappear then re-appear in a new location. This was repeated with a maximum of five different targets. The sequence of locations of the target were the same in each test. However, due to poor classification performance, the cursor control test was never fully completed by any of the subjects.

Subjects performed two rounds of the cursor control task, one using either classifier (multi-class, universum). The order of the two versions of the test was randomized.

In order to account for bias-shift caused by difference in the training and cursor testing environments (see \cref{fig:bshift}) the cursor control test was preceded by a short control phase where the subjects were asked to move the cursor in each direction for 10 seconds. The thresholds for each classifier were adjusted by a scalar value by an amount that maximised the total number of correct classifications. 

\begin{figure}[H]
	\centering
		\includegraphics[width=0.49\textwidth]{bshift.png}

	\caption{Possible bias-shift effect during testing}
	\label{fig:bshift}
	
\end{figure}

\subsection{Data collection -- cursor control}
Every $500$ ms, $2$ seconds of EEG data were collected and classified using the classifiers trained based on the training data (see following sections for further details regarding the classifiers). Based on the classification value, the cursor position was updated.

\subsection{Multi-class classifier}
The multi-class classifier consisted of two SVMs. These were trained using the labelled data gathered during the experiment. 
Half of the samples collected were removed for training the multi-class classifier in order to have the same amount of training data as for the universum classifier. The remaining data was divided into a training set consisting of 80\% of the data and a test set of 20\%. Training and test sets were also balanced.  
The first SVM was trained to distinguish between movement of a subject's left and right hand by classifying the EEG data collected during their left and right hand movements. Each training example consisted of 2 second of EEG data. 
K-fold cross validation was used to select the best performing hyper-parameters. 
The second SVM was trained to distinguish between the subject's tongue and feet movement in the same manner. 
EEG signals corresponding to one type of movement have many similarities to signals corresponding to other types of movement, resulting in cross-talk between the classifiers. 
To reduce cross-talk between the two classifiers, they were trained with distinct and combined examples: tongue/right hand, tongue/left hand, feet/right hand, and feet/left hand.

\begin{figure*}[H]
\subfloat[Multi-class vertical\label{fig:mver}]{%
\includegraphics[width=0.5\textwidth]{MVer.png}
}
\hfill
\subfloat[Multi-class horizontal\label{tab:adapt}]{%
\includegraphics[width=0.5\textwidth]{MHor.png}
}
\caption{Multi-class classifiers}
\label{fig:multi}
\end{figure*}

\subsection{Universum classifier}
The universum classifier also consists of two classifiers and was trained in the same manner as the multi-class classifier with one important difference: The universum classifier was not trained using combinations of movements and instead attempted to minimise cross-talking by training the vertical classifier to output zero for horizontal movement examples and training the horizontal classifier to output zero for vertical movement examples. 

\begin{figure}[H]
\subfloat[Universum vertical\label{fig:mver}]{%
\includegraphics[width=0.23\textwidth]{UVer.png}
}
\hfill
\subfloat[Universum horizontal\label{tab:adapt}]{%
\includegraphics[width=0.23\textwidth]{UHor.png}
}
\caption{Universum classifiers}
\label{fig:multi}
\end{figure}

\subsection{Data analysis}
\subsubsection{Accuracy}
In order to classify across each dimension two thresholds must be decided separating the three classes. Distributions for each class were assumed to be approximately Gaussian. For every subject for each classifier a Gaussian curve was fitted to the data for each class. The range for the positive class was from the intersection of the null class and positive class Gaussians to infinity. The range of the negative class was from the intersection of the null class and zero class Gaussians to negative infinity. The null class range was from the intersection of its Gaussian with the Gaussians of the other two classes. 
Each classifier was tested on the test set for that classifier and a result obtained in terms of percentage of accurate classifications. 

\subsubsection{Class independence}
Each of the two classifier in the universum and multi-class paradigms should ascribe a value between negative infinity and positive infinity to each classified example. A positive value indicates movement in one direction, a negative example indicates movement in the opposite direction and a value close to zero indicates no movement in that dimension. 
A threshold can be used to decide the range of each class. 
Ideally there will be no overlap between classes but because of cross-talk and artifacts this is not the case. The true range of values tend to be normally distributed and overlap each other. 
The multi-class and universum classifiers use different techniques to minimise cross-talk. 
The relative effectiveness of these techniques was evaluated by comparing the Bhattacharyya distance between the overlapping distributions. A larger distance equating to less cross-talk between the classes. This was calculated for vertical and horizontal dimensions in both classifiers using the following formula:

\begin{equation*}
   D_{B}(p,q) = 
\end{equation*}
\begin{equation*} 
    \frac{1}{4} \ln \left ( \frac{1}{4}\left( \frac{\sigma_{p}^{2}}{\sigma_{q}^{2}}+\frac{\sigma_{q}^{2}}{\sigma_{p}^{2}}+2\right ) \right ) +\frac{1}{4} \left ( \frac{(\mu_{p}-\mu_{q})^{2}}{\sigma_{p}^{2}+\sigma_{q}^{2}}\right )
\end{equation*}

In order to compare independence of classes using Battacharyya distance it was necessary to assume that all classes had the same number examples, which is not true. For instance, the horizontal classifier encountered double the number of vertical examples than left examples. This assumption meant that the Battacharyya distances measured were not the actual distances but were still able to be used for comparison because the same assumption was made for every classifier. 

\subsubsection{Cursor control}
One of the project objectives was to compare the performance of the multi-class versus the universum classifier with respect to the cursor control test. The two measures of cursor control performance were mean cursor speed -- calculating the average time taken to reach the target starting from the center of the screen -- and accuracy -- calculating the deviation from the optimal trajectory to the target.
However, as due to poor classifier performance none of the subjects succeeded in reaching the target, these measures were never actually calculated.