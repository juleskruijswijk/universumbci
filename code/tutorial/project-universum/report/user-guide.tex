\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage[english]{babel}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{float}
\usepackage{url}
\usepackage{a4wide}
\usepackage{listings}
\usepackage{tikz}
\usetikzlibrary{shapes,arrows,calc}
\usepackage{epstopdf}

\usepackage{array}
\setlength\extrarowheight{4pt}

\title{Universum 2-D Cursor Control \\ User \& Programming Guide}
\author{Laura Geurts (4419294) \\ Edward Grant (4482662) \\ Jules Kruijswijk (4140230) \\ Kevin Koschmieder (4218051)}
\date{\today}

\begin{document}
\maketitle
\tableofcontents

\newpage

\section{User Guide}

\subsection{Introduction}

% Include TITLE of BCI
This BCI has been developed by Laura Geurts, Edward Grant, Jules Kruijswijk and Kevin Koschmieder as part of the BCI practical course. 
The application was built to compare two different classification methods in a two-dimensional imagined movement control environment.
All files can be found online: \begin{quote}\url{https://bitbucket.org/juleskruijswijk/universumbci}.\end{quote}
Other than the subject number, no other settings require changing. The subject number is generated automatically during training. All signal processor files receive the subject number with an event. The remaining stimulus-files (\textit{univamTestStimulus.m, univamCursorStimulus}), \textit{univamTraining.m}, \textit{univamThreshold.m} and \textit{univamPartitionTest.m} inquire and check the applicability of a subject number using an input-prompt.
Next sections will give an overview to the basic use of the experiment.
The experiment consists of four steps:
\begin{itemize}
\item Preparation
\item Training Phase (\textit{univamCalibrateStimulus.m, univamCalibrateSignal.m})
\item Classifier Training
\begin{itemize}
\item Classifier Generation (\textit{univamTraining.m})
\item Threshold Calculation (\textit{univamThreshold.m})
\end{itemize}
\item Test Phase
\begin{itemize}
\item Arrow-Test (\textit{univamTestStimulus.m, univamTestSignal.m})
\item Cursor Control (\textit{univamCursorStimulus.m, univamCursorSignal.m})
\item Partition-Test (\textit{univamPartitionTest.m})
\end{itemize}
\end{itemize}

\subsection{Preparation}
Before any processes can be initiated, the subject has to be wired up using an EEG cap; in our experiment, a 64-channel BioSemi system was used.
To fit the cap, a few files should be started up in different terminal windows:
\begin{lstlisting}[language=bash]
sh /universumbci/code/dataAcq/startBuffer.sh
sh /universumbci/code/dataAcq/startBiosemi.sh
sh /universumbci/code/utilities/startSigViewer.sh
\end{lstlisting}
Using the signal viewer, the signals of the cap can be checked.
The 50Hz-Power option, that can be selected from the top-right drop down window, provides a good visualization of the signal quality. Ideally, every electrode would receive a signal in the green range; a greenish yellow is also sufficient.
If any electrode gives a bad signal, i.e. is in range between orange and red, additional gel can be applied to improve contact and subsequently, the signal. However, one should keep in mind that this does not always lead to a better signal.
One may close the window to proceed when everything is ready to go.
The terminal windows from the \textit{startBuffer.sh} and \textit{startBiosemi.sh}, on the other hand, have to remain open.

\subsection{Training}
To start the training phase, two MATLAB instances have to run in parallel.
In one MATLAB instance, run 
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamCalibrateSignal.m
\end{lstlisting}
In the second instance, run 
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamCalibrateStimulus.m
\end{lstlisting}
The subject/session number is automatically calculated, based on the existent \textit{training\_data.mat}- files in the data folder.
The second instance now shows a display with instructions, for the subject to read.
First, there will be a warm-up phase, where the subject can try out the different movements that he has to perform. This is done to familiarise the subject with the rather unusual movements, e.g. flexing the left hand, while moving the tongue with a closed mouth.
There are $8$ different movements that the user has to perform: left hand, right hand, foot, tongue, left hand \& tongue, left hand \& foot, right hand \& tongue and right hand \& foot.
A few pauses are included in the training phase to give the subject some rest. 
The training can be continued by pressing a button, preferably done by the experimenter.
At the end of the training, all the training data will be saved by the \textit{univamCalibrateSignal.m}.

\subsection{Classifier Training}
\textbf{Classifier Generation}\\
Use the saved data to train all the classifiers, for which you only need the MATLAB file name
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamTraining.m
\end{lstlisting}
Give in the subject number in the dialog box that pops up.
MATLAB will save all the classifiers that are trained, both multiclass and universum and for each a horizontal and vertical classifier.\\ \\
\textbf{Threshold Calculation}\\
Without thresholds, the application can only discern a positive and a negative class, but not a silent/zero condition, as the decision values are never exactly $0$. This file uses the generated classifiers and the training data to compute these thresholds between the three classes per classifier. To compute them, run
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamThreshold.m
\end{lstlisting}
One only has to enter the subject number in the dialog box at the beginning.

\subsection{Testing}

\textbf{Arrow Test}\\
In the test phase, two MATLAB instances are required.
In one MATLAB instance, run the file 
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamTestSignal.m
\end{lstlisting}
That is the signal processor, which records and classifies the data upon receiving stimulus-events.
In the other MATLAB instance, run the file 
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamTestStimulus.m
\end{lstlisting}
This file creates the stimulus, shows the subject which movement to perform and gives the subject feedback whether the BCI correctly recognizes the movement or in which mistaken direction the BCI interpreted the signal. The stimulus and sequence shown to the subject is shown in Figure ~\ref{fig:ArrowTestSequence}. Feedback is based on the universum classifiers, statistics are recorded for both classifier types. The subject number is entered into a dialog box at the beginning of the test.

\clearpage
\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{figures/arrow_test_seq.png}
\caption{Sequence in the Arrow Test. A yellow cue indicates the movement the subject has to perform, as soon as all arrows are white again. Then the system waits for a response from the classification pipeline to show the feedback, a green arrow in the same direction for a successful attempt, a red arrow in a different direction for a failed attempt, i.e. misclassification of movement.}
\label{fig:ArrowTestSequence}
\end{figure}
\noindent\textbf{Cursor Control}\\
For cursor control, again two MATLAB instances are required. In one MATLAB instance, run the file 
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamCursorSignal.m
\end{lstlisting}
That is the signal processor, which records and classifies the data continuously. Every $0.5$ seconds, a sample of $2$ seconds is classified. Additionally, it provides a visualization for the experimenter to see online what predictions the classifiers compute in relation to the thresholds (and later shifted to account for the new task).
In the other MATLAB instance, run the file 
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamCursorStimulus.m
\end{lstlisting}
This file creates and updates the cursor visualization. Additionally, it includes a pre-phase, in which a shift-value for both orientations is calculated. In this phase, the subject has to perform (visually aided) five taks for a brief amount of time: no movement, just tongue-, just right-hand-, just feet-, and just left-hand-movement. The subject number is entered into a dialog box at the beginning of the test.\\\\
\textbf{Partition Test}\\
To perform this test, run the file
\begin{lstlisting}[language=bash]
/universumbci/code/tutorial/project-universum/univamPartitionTest.m
\end{lstlisting}
The subject number has to be entered into a dialog box at the beginning of the test. Then the training data is split into a training set ($80\%$) and a test set ($20\%$). Classifiers are newly generated with this smaller training set, thresholds calculated and then the test set is fed into the classifiers. Statistics are recorded for predictions by the individual classifiers, as well as for their combined performance. 

\newpage

\section{Programmer Guide}

\subsection{Introduction}
This section will give an overview of the code that is used.
Firstly, flowcharts of the training and test phase will be given.
Secondly, a description of the purpose of all the matlab files that were used will be given.

\subsection{Training}

\tikzstyle{block} = [rectangle, draw, fill=blue!20, 
    text width=12em, text centered, rounded corners, minimum height=4em]
\tikzstyle{line} = [draw, -latex']

\begin{center}
\begin{tikzpicture}[node distance = 2cm, auto]
% Place nodes
\node [block] (inst) {Instructions\\ Training phase};
\node [block, below of=inst] (start) {Start sequence};
\node [block, below of=start] (disp) {Display stimulus (2s)};
\node [block, below of=disp] (blank) {Blank screen\\ (perform movement, 10s)};
\node [block, right of=blank, node distance = 7cm] (rec) {Record data (2s)};
\node [block, below of=rec] (save) {Save data};
\node [block, below of=blank] (halt) {Halt cue (3s)};
\node [block, below of=halt] (ends) {End of sequence};
\node [block, below of=ends] (endt) {End of training};
\node [block, below of=endt] (clas) {Train classifier};
\node [block, below of=clas] (thres) {Threshold calculator};
% Draw edges
\path [line] (inst) -- (start);
\path [line] (start) -- (disp);
\path [line] (disp) -- (blank);
\path [line] (blank) -- (halt);
\path [line] (halt) -- (ends);
\path [line] (ends) -- (endt);
\path [line] (endt) -- (clas);
\path [line] (clas) -- (thres);
\path [line] (halt.west)|-($(halt.west)-(1.5,0)$) |- node [align=center] {8x} (disp);
\path [line] (ends.west)|-($(ends.west)-(3,0)$) |- node [align=center] {30x} (start);
\path [line, dashed] (blank) -- node {5x} (rec.west);
\path [line, dashed] (save.south) |- (clas);
\path [line] (rec) -- (save);
\end{tikzpicture}
\end{center}

\subsection{Test}

\textbf{Arrow Test}
\begin{center}
\begin{tikzpicture}[node distance = 2cm, auto]
% Place nodes
\node [block] (inst) {Instructions \\ Arrow Test};
\node [block, below of=inst] (start) {Start sequence};
\node [block, below of=start] (disp) {Display cue};
\node [block, below of=disp] (blank) {Blank screen \\ (perform movement, 3sec)};
\node [block, right of=blank, node distance = 7cm] (rec) {Record data (2s)};
\node [block, below of=rec] (clas) {Classify};
\node [block, below of=blank] (upd) {Give feedback};
\node [block, below of=upd] (ends) {End of sequence};
\node [block, below of=ends] (ende) {End of experiment};
\node [block, below of=ende] (stats) {Save statistics};
% Draw edges
\path [line] (inst) -- (start);
\path [line] (start) -- (disp);
\path [line] (disp) -- (blank);
\path [line] (blank) -- (upd);
\path [line] (upd) -- (ends);
\path [line] (ends) -- (ende);
\path [line] (ende) -- (stats);
\path [line] (rec) -- (clas);
\path [line, dashed] (blank) -- node {after 500 ms} (rec.west);
\path [line, dashed] (clas.west) -- (upd);
\path [line] (upd.west)|-($(upd.west)-(1,0)$) |- node [align=center] {8x} (disp);
\path [line] (ends.west)|-($(ends.west)-(2,0)$) |- node [align=center] {10x} (start);
\end{tikzpicture}
\end{center}
\clearpage
\textbf{Cursor Control}
\begin{center}
\begin{tikzpicture}[node distance = 2cm, auto]
% Place nodes
\node [block] (inst) {Instructions \\ Cursor Control};
\node [block, below of=inst] (start) {Start sequence \\ 1. Shift calculation \\ 2. First Classifier \\ 3. Second Classifier};
\node [block, below of=start] (disp) {Display\\ cursor, target};
\node [block, right of=disp, node distance = 7cm] (rec) {Record data (2s)};
\node [block, below of=rec] (clas) {Classify};
\node [block, below of=disp] (upd) {Update cursor};
\node [block, below of=upd] (ends) {End of sequence};
\node [block, below of=ends] (ende) {End of experiment};
% Draw edges
\path [line] (inst) -- (start);
\path [line] (start) -- (disp);
\path [line] (disp) -- (upd);
\path [line] (upd) -- (ends);
\path [line] (ends) -- (ende);
\path [line] (rec) -- (clas);
\path [line, dashed] (disp) -- node {every 500 ms} (rec.west);
\path [line, dashed] (clas.west) -- (upd);
\path [line] (upd.west)|-($(upd.west)-(1,0)$) |- (disp);
\path [line] (ends.west)|-($(ends.west)-(2,0)$) |- (start);
\end{tikzpicture}
\end{center}
\clearpage

\subsection{Matlab-Files}
This section provides a general overview of the purpose of the various MATLAB-files and any insights into the workings, that require mention.\\

\noindent\textbf{univamCalibrateStimulus.m}\\
This file is used in the training phase to show the subject the stimulus cues, and by sending events initiates the data recordings. Visualization utilizes three axes, two for stimulus cues (if a diagonal direction, e.g. feet and right hand, is shown) and one for texts, e.g. instructions. Instructions are read from txt-files, can be altered outside of MATLAB.\\

\noindent\textbf{univamCalibrateSignal.m}\\
This file contains the signal processor used during training. It receives events denoting shown stimuli, records data from the EEG-signals and saves them in the end as a \textit{training\_data.mat}.\\

\noindent\textbf{univamTraining.m}\\
This file creates all necessary classifiers, i.e. for both classifier types a horizontal and a vertical classifier. For the universum classifiers, only straight directions are considered in the process, i.e. up, left, down, and right. The multi-class includes all eight directions in the generation process. To account for the difference in samples available for the respective classifiers, the multi-class classifiers are only trained on the first half of the data samples. Furthermore, before any classifier can be trained, the \textit{devents} must be re-labelled for the respective methods:
\begin{itemize}
\item Universum
\begin{itemize}
\item $-1$: negative class
\item $0$: ignored class
\item $1$: positive class
\item $2$: universum class
\end{itemize}
\item Multi-class
\begin{itemize}
\item $-1$: negative class
\item $0$: ignored class
\item $1$: positive class
\end{itemize}
\end{itemize}

\noindent\textbf{univamThreshold.m}\\
This file calculates the thresholds between the negative and zero class, and the positive and zero class. To do this, the part of training data, used to train the classifiers, representing the straight directions is classified with all four classifiers: universum vertical, universum horizontal, multi-class vertical, and multi-class horizontal. Under the assumption that the decision values for the different classes are normally distributed given enough samples, normal distributions of the three classes are calculated. Their intersections are taken as the threshold, if the intersection is between both means. If the means of the three distributions are not in order ( mean(positive class) $>$ mean(zero class) $>$ mean(negative class)), only a border between positive and negative class is calculated and the zero-class omitted. Note: this procedure should be revised, as it is not optimal.\\

\noindent\textbf{univamTestStimulus.m}\\
This file contains the visualization of the arrow test. It shows the subject cues to initiate a movement, send events and generates on the basis of received predictions feedback and statistics. All cues and feedbacks are images created outside of MATLAB.\\

\noindent\textbf{univamTestSignal.m}\\
This is the signal processor for the arrow test. Calculates classifications that are used for feedback.\\

\noindent\textbf{univamCursorStimulus.m}\\
This file visualises the cursor control. First of all, the subject has to provide information about a potential shift in brain-signals due to the fundamentally different task. In this pre-phase, a shift is calculated by averaging classifier predictions over four conditions: up, right, down, and left. Then, two phases for the different classifier types follow. The subject is instructed to move the cursor (by moving the specific body parts for the desired directions) to the target. If the target is reached, it is moved. Performance of the cursor control never warranted any implementation of statistical recordings.\\

\noindent\textbf{univamCursorSignal.m}\\
Signal processor for cursor control. Sends a classification of a 2-second-segment of brain signals every half a second to the animation-updater in \textit{univamCursorStimulus.m}. Additionally, it provides a feedback visualisation for the experimenter, as you can see in Figure ~\ref{fig:Visual}.\\

\begin{figure}
\centering
\includegraphics[width=0.9\textwidth]{figures/FeedbackGraphs.eps}
\caption{Visualisation of the classifier predictions (vertical on the left, horizontal on the right) in relation to their respective thresholds. After the pre-phase, they are adjusted according to the shift.}
\label{fig:Visual}
\end{figure}

\noindent\textbf{univamPartitionTest.m}\\
Partitions the training data into a training and a test set. Then, it trains the classifiers, computes the thresholds and tests them. Statistics of the all classifiers, individually and in collaboration, are displayed in the command line. The code is collected from other existing files (\textit{univamTraining.m}, \textit{univamThreshold.m}, \textit{univamTestStimulus.m}).\\
\clearpage
\noindent\textbf{Helper Functions}
\begin{itemize}
\item readInstr(nInstr, classString)
\item dispStim(cueDB,combo,axis\_one,axis\_two)
\item calcThres(norm1, norm2, norm3)
\item getDir(decValueX, decValueY, threshold)
\item dispCursor(pred, cursor, target, tnm, axis\_one, axis\_two)
\end{itemize}

\subsubsection{State variables and Containers}

\begin{tabular}{p{4cm}p{9cm}}
\textbf{Variable} & \textbf{Purpose} \\
\hline
\hline
stimDur & duration of stimulus presentation\\
haltDur & duration of pause after movement\\
moveDur & duration of movement\\
moveDur1 & duration of movement pre-sendEvent (univamTest)\\
moveDur2 & duration of movement post-sendEvent (univamTest)\\
\hline
nSubj & number of subject (since subjects were trained more than once, it is more accurate to say number of session)\\
nSeq & number of sequences (i.e. round of all stimuli shown) for training or testing\\
nSymbs & number of different stimuli (in our case 8 stimuli for 8 directions)\\
nDP & total number of recorded data points per subject \\
hDP & number of recorded data points for one stimulus/direction (per subject) \\
\hline
cueDB & contains the image and position of the different stimuli (e.g. tongue-picture, top of screen) \\
eventDB & contains strings of all events \\
imageDB & contains images for all test configurations (8+1 directions, true/false, command/feedback) for the arrow-test \\
cursor & contains cursor image and position (initial/current)\\
target & contains target image and several positions for cursor control\\
\hline
sdata & contains recorded data \\
sdevents & contains event information for the recorded data, e.g. value (in our case: direction) \\
hdr & contains additional information for the data, e.g. electrode positions \\
\end{tabular}

\begin{tabular}{p{4cm}p{9cm}}
\textbf{Variable} & \textbf{Purpose} \\
\hline
\hline
deventsUNIv/h & container for specific labels needed for universum classification [-1:negative class, 0:ignored class, 1:positive class, 2:universum] \\
deventsMC8v/h & container for specific labels needed for multi-class classification [-1:negative class, 0:ignored class, 1:positive] \\
\hline
dirTable & reference table, that returns the direction number for the corresponding indices, e.g. dirTable(2,3)=2 : left \\
indTable & reference table, that returns the classes (vertical and horizontal) for a given direction number, e.g. indTable(5) = [1, 3], with [1:positive class, 2:zero class, 3:negative class], i.e. up and left \\
\hline
clsfrUNIv/h & universum classifiers for the vertical or horizontal orientation, respectively \\
clsfrMC8v/h & multi-class classifiers for the vertical or horizontal orientation, respectively \\
\hline
thresUNI & contains thresholds between all two neighbouring class for the horizontal and vertical universum classifiers \\
thresMC8 & contains thresholds between all two neighbouring class for the horizontal and vertical multi-class classifiers \\
shiftV/H & calculated shift in vertical and horizontal to adjust for new task (cursor control)\\
\hline
xyData & several containers for data of specific directions for purposes of threshold calculation, partition testing and cursor control checks \\
\hline
testDoc & records position of cursor and target each step during the cursor control test \\
\hline
predH/V & keep track of the last 100 classifications during cursor control for visualization purposes
\end{tabular}

\subsubsection{Events}

\begin{tabular}{p{3.5cm}|p{3cm}|p{6.5cm}}
\textbf{Event (univam.xxx)} & \textbf{Value} & \textbf{Purpose} \\
\hline
\hline
Subject & nSubj & sends the subject (more accurate session, if one subject performs multiple sessions) number to the corresponding signal processor; there the respective classifiers can then be loaded\\
\hline
TrainStart & 'start' & no purpose currently\\
TrainStimulus & eventDB\{nmb\} & signals start point and value of a recording \\
TrainEnd & 'end' & causes the signal processor to end and save the recorded training data\\
\hline
TestStart & 'start' & no purpose currently\\
TestStimulus & eventDB\{nmb\} & signals start point and value of a recording that is supposed to be classified\\
Pred.uni & \parbox[t]{3cm}{[Dec.Value Vert,\\ Dec.Value Hori]}  & sends predictions of the two universum classifiers for feedback and statistical records\\
Pred.mc8 & \parbox[t]{3cm}{[Dec.Value Vert,\\ Dec.Value Hori]}  & sends predictions of the two multi-class classifiers for feedback and statistical records\\
TestEnd & 'end' & causes the signal processor to end\\
\hline
SigProcReady & 'go' & allows the signal processor to catch up, before the cursor control application starts\\
CursorStart & 'uni' or 'mc8' & signals the beginning of the cursor control application and which classifier is supposed to be used\\
Thres.Hori/Vert & \parbox[t]{3cm}{[upperThreshold,\\ lowerThreshold]} & provides the signal processor with the thresholds for a visualization of the decision values  \\
Shift & \parbox[t]{3cm}{[Shift Hori,\\ Shift Vert]} & provides the calculated shift-values for the visualization of the decision values\\
Pred & \parbox[t]{3cm}{[Dec.Value Vert,\\ Dec.Value Hori]} & sends predictions for cursor update\\
CursorEnd & 'end' & signal end of cursor application and causes signal processor to end
\end{tabular}



\end{document}