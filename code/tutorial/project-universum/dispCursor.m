function [cursor, target, tnm] = dispCursor(pred, cursor, target, tnm, axis_one, axis_two)

% Pred is an array with two values for the vertical and horizontal
% prediction; values should be +1, 0, or -1
%
% cursor keeps the image of the cursor
%
% target keeps the image of the target
%
% axis_one displays the cursor
%
% axis_two displays the target
% 
% This function updates the cursor display each step.

    % Change Cursor Position
    if isempty(find(pred == 0))
        cursor{1,2} = cursor{1,2} + ([.05 .0 .0 .0] * 0.07 * pred(1));
        cursor{1,2} = cursor{1,2} + ([.0 .05 .0 .0] * 0.07 * pred(2));
    else
        cursor{1,2} = cursor{1,2} + ([.05 .0 .0 .0] * 0.1 * pred(1));
        cursor{1,2} = cursor{1,2} + ([.0 .05 .0 .0] * 0.1 * pred(2));
    end
    cond_1 = find(cursor{1,2} > 0.9);
    cond_2 = find(cursor{1,2} < 0.1);
    if ~isempty(cond_1)
        cursor{1,2}(cond_1) = 0.9;
    elseif ~isempty(cond_2)
        cursor{1,2}(cond_2) = 0.1;
    end
    % Test if Target was hit
    cent_cur = cursor{1,2}(1:2) + (cursor{1,2}(3:4) * 0.5);
    cent_tar = target{1,2}(1:2) + (target{1,2}(3:4) * 0.5);
    dist = pdist([cent_cur; cent_tar]);
    if dist < 0.09
        target{1,2} = target{1,tnm+2};
        tnm = tnm + 1;
    end
    % Display
    axes(axis_one);
    set(gca,'position',target{1,2});
    htar = imshow(target{1,1});
    set(htar, 'AlphaData', target{2,2});
    axes(axis_two);
    set(gca,'position',cursor{1,2});
    hcur = imshow(cursor{1,1});
    set(hcur, 'AlphaData', cursor{2,2});
    drawnow;
end