run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

trlen_ms=2000;
% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

ongoingTest = true;
% Name for save file
validSubj = true;
nSubj = 0;
state = [];
while ~validSubj
    [devents,state]=buffer_newevents(buffhost,buffport,state,{'univam.Subject'},{},1000);
    if ~isempty(devents)
        nSubj = devents(1).value;
        validSubj = false;
    end
end
fileNameClsfrUNIv = ['data/subject_' num2str(nSubj) '/universum/clsfr_vert' ];
fileNameClsfrUNIh = ['data/subject_' num2str(nSubj) '/universum/clsfr_hori' ];
fileNameClsfrMC8v = ['data/subject_' num2str(nSubj) '/multi_class_8/clsfr_vert' ];
fileNameClsfrMC8h = ['data/subject_' num2str(nSubj) '/multi_class_8/clsfr_hori' ];
if ~(exist([fileNameClsfrUNIv '.mat'], 'file') && exist([fileNameClsfrUNIh '.mat'], 'file') && ...
        exist([fileNameClsfrMC8v '.mat'], 'file') && exist([fileNameClsfrMC8h '.mat'], 'file'))
    error('Not all classifiers available!');
end
cslfrUNIv = load(fileNameClsfrUNIv);
cslfrUNIh = load(fileNameClsfrUNIh);
cslfrMC8v = load(fileNameClsfrMC8v);
cslfrMC8h = load(fileNameClsfrMC8h);

while ongoingTest 
    [data,devents,state] = buffer_waitData(buffhost,buffport,state,'startSet',...
        {'univam.TestStimulus'},'trlen_ms',trlen_ms,'exitSet',...
        {'data' {'univam.TestEnd'} 'end'})
    
    for ei=1:numel(devents)
        if (matchEvents(devents(ei), 'univam.TestEnd', 'end'))
            ongoingTest = false;
        else
            [f_v,~,~]=buffer_apply_ersp_clsfr(data.buf,cslfrUNIv);
            [f_h,~,~]=buffer_apply_ersp_clsfr(data.buf,cslfrUNIh);
            sendEvent('univam.Pred.uni', [f_v, f_h]);
            [f_v,~,~]=buffer_apply_ersp_clsfr(data.buf,cslfrMC8v);
            [f_h,~,~]=buffer_apply_ersp_clsfr(data.buf,cslfrMC8h);
            sendEvent('univam.Pred.mc8', [f_v, f_h]);
        end
    end
end
