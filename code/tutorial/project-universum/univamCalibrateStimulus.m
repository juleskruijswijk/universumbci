run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% set the real-time-clock to use
initgetwTime;
initsleepSec;

% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

% Time Database
stimDur = 2.0; % Duration of stimulus
haltDur = 3.0; % Duration of halt cue
moveDur = 10.0; % Duration of movement

nSeq = 10;     % Number of sequences (one sequence := nSymbs stimuli)
nSymbs = 8;    % Number of directions / stimuli

% String Database 
instr1 = readInstr(4,'univam1');
instr2 = readInstr(1,'univam2');
instr3 = readInstr(1,'univam3');
warmUp = {'WARM UP PHASE','','Press any button to start!'};
train = {'TRAINING PHASE','','Press any button to start!'};
test = {'TESTING PHASE','','Press any button to start!'};
clsfr_txt{1} = {'First Classifier Test','','Press any button to start!'};
clsfr_txt{2} = {'Second Classifier Test','','Press any button to start!'};

% Image Database
cueDB{1,1} = imread('images/tongue.png');
cueDB{1,2} = [.35 .7 .3 .3];
cueDB{2,1} = flipdim(imread('images/hand.png'),2);
cueDB{2,2} = [.0 .35 .3 .3];
cueDB{3,1} = imread('images/feet.png');
cueDB{3,2} = [.35 .0 .3 .3];
cueDB{4,1} = imread('images/hand.png');
cueDB{4,2} = [.7 .35 .3 .3];
cueDB{5,1} = imread('images/halt.png');
cueDB{5,2} = [.4 .4 .2 .2];

% Event Database
eventDB = {'up', 'left', 'down', 'right', 'upleft', 'downleft',...
    'downright', 'upright'};

% GUI
pos_std = [.1 .1 .8 .8];
clf;
set(gcf,'color',[0 0 0],'toolbar','none','menubar','none',...
    'units','normalized','position',[0 0 1 1]);
ax1 = axes('visible','off','color',[0 0 0],'units','normalized','position',...
    pos_std);
ax2 = axes('visible','off','color',[0 0 0],'units','normalized','position',...
    pos_std);
ax3 = axes('visible','off','color',[0 0 0],'units','normalized','position',...
    pos_std);
axes(ax1);
tb_1=text(0.15,.9,'text','HorizontalAlignment','left','VerticalAlignment',...
    'top','FontUnits','normalized','fontsize',.04,'color',[1 1 1],...
    'visible','off');
tb_2=text(.5,.5,'text','HorizontalAlignment','center','VerticalAlignment',...
    'middle','FontUnits','normalized','fontsize',.10,'color',[1 1 1],...
    'visible','off');

% Subject Number Input
nSubj = 0;
validSubj = false;
while ~validSubj
    nSubj = nSubj + 1;
    folder_name = ['data/subject_' num2str(nSubj)];
    mkdir(folder_name);
    fileNameTD = ['data/subject_' num2str(nSubj) '/training_data'];
    if ~exist([fileNameTD '.mat'], 'file')
        validSubj = true;
        sendEvent('univam.Subject', nSubj);
    end
end
    
% INSTRUCTIONS
for ii=1:numel(instr1)
    set(tb_1,'string',instr1{ii},'visible','on');
    drawnow; waitforbuttonpress;
end
set(tb_1,'visible','off');
% WARM UP
% Familiarize the subject with the build-up of an epoch for every
% possible movement (tongue, left hand, right hand, and feet)
set(tb_2,'string',warmUp,'visible','on');
drawnow; waitforbuttonpress;
set(tb_2,'visible','off');
axes(ax2);
for i=1:8
    % Display stimulus
    [himg_1,himg_2] = dispStim(cueDB,i,ax2,ax3);
    drawnow; sleepSec(stimDur);
    % Movement time
    set(himg_1,'visible','off');
    set(himg_2,'visible','off');
    drawnow; sleepSec(moveDur);
    % Display Halt Cue
    set(gca,'position',cueDB{5,2});
    himg = imshow(cueDB{5,1});
    drawnow; sleepSec(haltDur);
end
% TRAINING PHASE
set(himg,'visible','off');
%Display instructions
axes(ax1);
for ii=1:numel(instr2)
    set(tb_1,'string',instr2{ii},'visible','on');
    drawnow; waitforbuttonpress;
end
set(tb_1,'visible','off');
set(tb_2,'string',train,'visible','on');
drawnow; waitforbuttonpress;
set(tb_2,'visible','off');
axes(ax2);
drawnow;
sendEvent('univam.TrainStart', 'start');
sleepSec(1);
for ei=1:nSeq
    tgtSeq=mkStimSeqRand(nSymbs,nSymbs);
    for ej=1:nSymbs
        % Find stimulus in the target sequence
        nmb = find(tgtSeq(:,ej));
        % Display stimulus
        [himg_1,himg_2] = dispStim(cueDB,nmb,ax2,ax3);
        drawnow; sleepSec(stimDur);
        % Movement time
        set(himg_1,'visible','off');
        set(himg_2,'visible','off');
        drawnow; 
        % Send Event
        for evi=1:5
            sendEvent('univam.TrainStimulus', eventDB{nmb});
            sleepSec(2);
        end
        % Display halt cue
        set(gca,'position',cueDB{5,2});
        himg = imshow(cueDB{5,1});
        drawnow; sleepSec(haltDur);
        set(himg,'visible','off');
    end
    % Breaks
    if mod(ei, 2)==0
       axes(ax1);
       set(tb_2,'string',{'Press button' 'if you are ready to continue!'}...
           ,'visible','on');
       drawnow; waitforbuttonpress;
       set(tb_2,'visible','off');
       axes(ax2);
    end
end
sendEvent('univam.TrainEnd', 'end');
close;
