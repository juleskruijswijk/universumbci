run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

trlen_ms=2000;
% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

ongoingTrain = true;
% Name for save file
validSubj = true;
nSubj = 0;
state = [];
while validSubj
    [devents,state]=buffer_newevents(buffhost,buffport,state,{'univam.Subject'},{},1000);
    if ~isempty(devents)
        nSubj = devents(1).value;
        validSubj = false;
    end
end
fileNameTD = ['data/subject_' num2str(nSubj) '/training_data' ];
% Containers for saving
sdata = struct;
sdevents = struct;
counter = 0;

state = [];

while ongoingTrain 
    [data,devents,state]=buffer_waitData(buffhost,buffport,state,'startSet',...
        {'univam.TrainStimulus'},'trlen_ms',trlen_ms,'exitSet',...
        {'data' {'univam.TrainEnd'} 'end'})
    
    for ei=1:numel(devents)
        if (matchEvents(devents(ei), 'univam.TrainEnd', 'end'))
            ongoing_train = false;
        else
            counter = counter + 1;
            if counter == 1
                sdata = data(ei);
                sdevents = devents(ei);
            else
                sdata(counter) = data(ei);
                sdevents(counter) = devents(ei);
            end
        end
    end
end

save(fileNameTD, 'sdata', 'sdevents','hdr');


