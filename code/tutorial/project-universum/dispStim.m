function [himg_1,himg_2] = dispStim(cueDB,combo,axis_one,axis_two)
%   Function displays the various single or pairs of stimuli in their
%   respective spot, e.g. image of tongue shown on the upper middle part 
%   of the screen.
    switch combo
        case {1, 2, 3, 4}
            axes(axis_one);
            set(gca,'position',cueDB{combo,2});
            himg_1 = imshow(cueDB{combo,1});
            himg_2 = [];
        case {5, 6, 7}
            axes(axis_one);
            set(gca,'position',cueDB{combo-4,2});
            himg_1 = imshow(cueDB{combo-4,1});
            axes(axis_two);
            set(gca,'position',cueDB{combo-3,2});
            himg_2 = imshow(cueDB{combo-3,1});
        case 8
            axes(axis_one);
            set(gca,'position',cueDB{combo-4,2});
            himg_1 = imshow(cueDB{combo-4,1});
            axes(axis_two);
            set(gca,'position',cueDB{1,2});
            himg_2 = imshow(cueDB{1,1});
    end
end