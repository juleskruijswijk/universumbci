run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;


% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

ongoingTest = false;
waitTest = true;
% Name for classifier file
waitSubj = true;
nSubject = 0;
state = [];
while waitSubj
    [devents,state]=buffer_newevents(buffhost,buffport,state,{'univam.Subject'},{},1000);
    if ~isempty(devents)
        nSubject = devents(1).value;
        waitSubj = false;
    end
end
fileNameClsfrUNIv = ['data/subject_' num2str(nSubject) '/universum/clsfr_vert' ];
fileNameClsfrUNIh = ['data/subject_' num2str(nSubject) '/universum/clsfr_hori' ];
%fileNameClsfrMC4v = ['data/subject_' num2str(nSubject) '/multi_class_4/clsfr_vert' ];
%fileNameClsfrMC4h = ['data/subject_' num2str(nSubject) '/multi_class_4/clsfr_hori' ];
fileNameClsfrMC8v = ['data/subject_' num2str(nSubject) '/multi_class_8/clsfr_vert' ];
fileNameClsfrMC8h = ['data/subject_' num2str(nSubject) '/multi_class_8/clsfr_hori' ];
% Times
timeout_ms=5000;
trlen_ms=2000;
trlen_samp = round(trlen_ms*hdr.fSample/1000);
step_ms=500;
step_samp  = round(step_ms*hdr.fSample/1000);
state=[]; % for the buffer_newevents waiting for exit event
state2=[];
nEvents=hdr.nEvents; nSamples=hdr.nSamples; % current number events/samples

fileNameTD = ['data/subject_' num2str(nSubject) '/training_data'];

load(fileNameTD);
% Used to check the workings of the cursor animation
llData = sdata(find(cellfun( @(x) strcmp(x,'left'), {sdevents.value})));
rrData = sdata(find(cellfun( @(x) strcmp(x,'right'), {sdevents.value})));
uuData = sdata(find(cellfun( @(x) strcmp(x,'up'), {sdevents.value})));
ddData = sdata(find(cellfun( @(x) strcmp(x,'down'), {sdevents.value})));

predH = zeros(1,100);
predV = zeros(1,100);
thres = zeros(4,100);
shift = [0 0];
x = 1:1:100;
counter = 0;
sendEvent('univam.SigProcReady', 'go');
while waitTest
    % Load classifier
    [devents,state]=buffer_newevents(buffhost,buffport,state,...
        {'univam.CursorStart'},{},1000);
    if ( numel(devents) )
        ongoingTest = true;
        waitTest = false;
        if matchEvents(devents(1),'univam.CursorStart','uni')
            clsfrV = load(fileNameClsfrUNIv);
            clsfrH = load(fileNameClsfrUNIh);
        elseif matchEvents(devents(1),'univam.CursorStart','mc4')
            clsfrV = load(fileNameClsfrMC4v);
            clsfrH = load(fileNameClsfrMC4h);
        elseif matchEvents(devents(1),'univam.CursorStart','mc8')
            clsfrV = load(fileNameClsfrMC8v);
            clsfrH = load(fileNameClsfrMC8h);
        end
    end
end
while ongoingTest
    [devents, state2]=buffer_newevents(buffhost,buffport,state2,...
        {'univam.Thres.Hori' 'univam.Thres.Vert' 'univam.Shift'},{},0);
    if (~isempty(devents))
        for ei=1:numel(devents)
            if matchEvents(devents(ei), 'univam.Thres.Hori')
                thres(1,:) = devents(ei).value(1);
                thres(2,:) = devents(ei).value(2);
            elseif matchEvents(devents(ei), 'univam.Thres.Vert')
                thres(3,:) = devents(ei).value(1);
                thres(4,:) = devents(ei).value(2);
            elseif matchEvents(devents(ei), 'univam.Shift')
                shift = devents(ei).value;
            end
        end
    end
    status=buffer('wait_dat',[nSamples+trlen_samp -1 timeout_ms],buffhost,buffport);
    if ( status.nsamples < nSamples )
        fprintf('Buffer restart detected!');
        nSamples=status.nsamples;
        continue;
    end
    onSamples=nSamples;
    start = onSamples:step_samp:status.nsamples-trlen_samp-1; % window start positions
    if( ~isempty(start) )
        nSamples=start(end)+step_samp;
    end
    for si = 1:numel(start)
        %data = buffer('get_dat',[start(si) start(si)+trlen_samp-1],buffhost,buffport);
        
        if (counter/50)<1
            data = uuData(randi(30));
        elseif (counter/50)<2
            data = rrData(randi(30));
        elseif (counter/50)<3
            data = ddData(randi(30));
        elseif (counter/50)<4
            data = llData(randi(30));
        else
            counter = 0;
        end
        counter = counter+1;
        [f_v,~,~]=buffer_apply_ersp_clsfr(data.buf,clsfrV);
        [f_h,~,~]=buffer_apply_ersp_clsfr(data.buf,clsfrH);
        sendEvent('univam.Pred', [f_v, f_h]);
        % Visualization of prediction values
        predH(2:100) = predH(1:99);
        predH(1) = f_h - shift(1);
        predV(2:100) = predV(1:99);
        predV(1) = f_v - shift(2);
        figure(1);
        subplot(4,7,[6:7,13:14,20:21,27:28]);
        plot(predH,x,'b');hold on;
        plot(thres(1,:),x,'g');hold on;
        plot(thres(2,:),x,'r');hold off;
        title('Horizontal Classification');
        xlabel('Prediction Value (shifted)');
        ylabel('Steps Back');
        set(gca,'YDir','reverse');
        subplot(4,7,[8:11,15:18]);
        plot(x,predV,'b');hold on;
        plot(x,thres(3,:),'g');hold on;
        plot(x,thres(4,:),'r');hold off;
        title('Vertical Classification');
        xlabel('Steps Back');
        ylabel('Prediction Value (shifted)');
        drawnow;
    end
    % check for exit events
    [devents,state]=buffer_newevents(buffhost,buffport,state,{'univam.CursorEnd'},{'end'},0);
    if ( numel(devents)>0 )
        ongoingTest=false;
    end;
end



