run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% Constants
capFile='1010.txt';
overridechnm=1; % capFile channel names override those from the header!

% IO-Variables
validSubj = false;
nSubj = 0;
while ~validSubj
    prompt = {'Enter Subject Number:'};
    dlg_title = 'Input';
    num_lines = 1;
    def = {'0'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    
    fileNameTD = ['data/subject_' num2str(answer{1,1}) '/training_data' ];
    if exist([fileNameTD '.mat'], 'file') 
        validSubj = true;
        nSubj = answer{1,1};
        folderUNI = ['data/subject_' num2str(answer{1,1}) '/universum'];
        mkdir(folderUNI);
%        folderMC4 = ['data/subject_' num2str(answer{1,1}) '/multi_class_4'];
%        mkdir(folderMC4);
        folderMC8 = ['data/subject_' num2str(answer{1,1}) '/multi_class_8'];
        mkdir(folderMC8);
    else
        uiwait(msgbox('No training data for this subject available!', 'Error','error'));
    end
end

folderSubj = ['data/subject_' num2str(nSubj) '/'];
fileNameTD  = [folderSubj 'training_data'];

% Load Data and generate devents-files for the various classifiers
load(fileNameTD);
nDP = numel(sdevents);

deventsUNIv = sdevents;
deventsUNIh = sdevents;
% deventsMC4v = sdevents;
% deventsMC4h = sdevents;
deventsMC8v = sdevents;
deventsMC8h = sdevents;
% Re-label Universum
% positive class:    1:= up for vert, right for hori
% negative class:   -1:= down for vert, left for hori
% universum class:   2:= left/right for vert, up/down for hori 
%
% ignored class:     0:= all diagonal directions
%
% Re-label Multiclass (4 directions)
% positive class:    1:= up for vert, right for hori
% negative class:   -1:= down for vert, left for hori
%
% ignored class:     0:= left/right for vert, up/down for hori, 
%                           all diagonal directions
%
% Re-label Multiclass (8 directions)
% positive class:    1:= all cases containing up for vert, all cases
%                           containing right for hori
% negative class:   -1:= all cases containing down for vert, all cases
%                           containing left for hori
% ignored class:     0:= pure left/right for vert, pure up/down for hori
for ei=1:numel(sdevents)
    switch sdevents(ei).value
        case 'up'
            % Universum
            deventsUNIv(ei).value =  1;
            deventsUNIh(ei).value =  2;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  1;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  1;
            deventsMC8h(ei).value =  0;
        case 'left'
            % Universum
            deventsUNIv(ei).value =  2;
            deventsUNIh(ei).value =  -1;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value = -1;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  0;
            deventsMC8h(ei).value = -1;
        case 'right' % is back to correct version
            % Universum
            deventsUNIv(ei).value =  2;
            deventsUNIh(ei).value =  1;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  1;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  0;
            deventsMC8h(ei).value =  1;
        case 'down' % is back to correct version
            % Universum
            deventsUNIv(ei).value =  -1;
            deventsUNIh(ei).value =  2;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value = -1;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  -1;
            deventsMC8h(ei).value =  0;
        case 'upleft'
            % Universum
            deventsUNIv(ei).value =  0;
            deventsUNIh(ei).value =  0;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  1;
            deventsMC8h(ei).value = -1;
        case 'downleft'
            % Universum
            deventsUNIv(ei).value =  0;
            deventsUNIh(ei).value =  0;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  -1;
            deventsMC8h(ei).value =  -1;
        case 'downright'
            % Universum
            deventsUNIv(ei).value =  0;
            deventsUNIh(ei).value =  0;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  -1;
            deventsMC8h(ei).value =  1;
        case 'upright'
            % Universum
            deventsUNIv(ei).value =  0;
            deventsUNIh(ei).value =  0;
            % Multiclass (4 directions)
%             deventsMC4v(ei).value =  0;
%             deventsMC4h(ei).value =  0;
            % Multiclass (8 directions)
            deventsMC8v(ei).value =  1;
            deventsMC8h(ei).value =  1;
    end
end

% train classifier
% universum classifiers  
fprintf('Generating classifier for universum/vertical \n');
clsfrUNIv = buffer_train_ersp_clsfr(sdata(1:4/5*nDP),deventsUNIv(1:4/5*nDP),hdr,'objFn','universum','spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
fprintf('Generating classifier for universum/horizontal \n');
clsfrUNIh = buffer_train_ersp_clsfr(sdata(1:4/5*nDP),deventsUNIh(1:4/5*nDP),hdr,'objFn','universum','spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
% multiclass classifiers (8 directions)
fprintf('Generating classifier for 8-multiclass/vertical \n');
clsfrMC8v = buffer_train_ersp_clsfr(sdata(1:2/5*nDP),deventsMC8v(1:2/5*nDP),hdr,'spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
fprintf('Generating classifier for 8-multiclass/horizontal \n');
clsfrMC8h = buffer_train_ersp_clsfr(sdata(1:2/5*nDP),deventsMC8h(1:2/5*nDP),hdr,'spatialfilter','slap','freqband',...
    [6 10 26 30],'badchrm',0,'capFile',capFile,'overridechnms',overridechnm,'binsp',0);
close all;

% Create Train and Test Set
dataTrainPart = sdata(1:4/5*nDP);
deventsTrainPart = sdevents(1:4/5*nDP);

dataTestPart = sdata((4/5*nDP)+1:nDP);
deventsTestPart = sdevents((4/5*nDP)+1:nDP);


% Separation of train set data 
llData = dataTrainPart(find(cellfun( @(x) strcmp(x,'left'), {deventsTrainPart.value})));
rrData = dataTrainPart(find(cellfun( @(x) strcmp(x,'right'), {deventsTrainPart.value})));
uuData = dataTrainPart(find(cellfun( @(x) strcmp(x,'up'), {deventsTrainPart.value})));
ddData = dataTrainPart(find(cellfun( @(x) strcmp(x,'down'), {deventsTrainPart.value})));

hDP = numel(dataTrainPart)/8;
% Vertical
predUNIv = zeros(4*hDP,2);
predUNIv(1:hDP,1) = 1;
predUNIv(hDP+1:3*hDP,1) = 0;
predUNIv(3*hDP+1:4*hDP,1) = -1;
predMC8v = zeros(4*hDP,2);
predMC8v(1:4*hDP,1) = 1;
predMC8v(hDP+1:3*hDP,1) = 0;
predMC8v(3*hDP+1:4*hDP,1) = -1;
for i=1:hDP
    [f_v,~,~]=buffer_apply_ersp_clsfr(uuData(i).buf,clsfrUNIv);
    predUNIv(i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(llData(i).buf,clsfrUNIv);
    predUNIv(hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(rrData(i).buf,clsfrUNIv);
    predUNIv(2*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(ddData(i).buf,clsfrUNIv);
    predUNIv(3*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(uuData(i).buf,clsfrMC8v);
    predMC8v(i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(llData(i).buf,clsfrMC8v);
    predMC8v(hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(rrData(i).buf,clsfrMC8v);
    predMC8v(2*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(ddData(i).buf,clsfrMC8v);
    predMC8v(3*hDP+i,2) = f_v;
end

% Horizontal
predUNIh = zeros(4*hDP,2);
predUNIh(1:hDP,1) = 1;
predUNIh(hDP+1:3*hDP,1) = 0;
predUNIh(3*hDP+1:4*hDP,1) = -1;
predMC8h = zeros(4*hDP,2);
predMC8h(1:hDP,1) = 1;
predMC8h(hDP+1:3*hDP,1) = 0;
predMC8h(3*hDP+1:4*hDP,1) = -1;
for i=1:hDP
    [f_v,~,~]=buffer_apply_ersp_clsfr(rrData(i).buf,clsfrUNIh);
    predUNIh(i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(uuData(i).buf,clsfrUNIh);
    predUNIh(hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(ddData(i).buf,clsfrUNIh);
    predUNIh(2*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(llData(i).buf,clsfrUNIh);
    predUNIh(3*hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(rrData(i).buf,clsfrMC8h);
    predMC8h(i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(uuData(i).buf,clsfrMC8h);
    predMC8h(hDP+i,2) = f_v;
    [f_v,~,~]=buffer_apply_ersp_clsfr(ddData(i).buf,clsfrMC8h);
    predMC8h(2*hDP+i,2) = f_v;
    [f_v,fraw_v,p_v]=buffer_apply_ersp_clsfr(llData(i).buf,clsfrMC8h);
    predMC8h(3*hDP+i,2) = f_v;
end

% Threshold calculation
thresUNI = [0 0; 0 0]; % upper row horizontal
thresMC8 = [0 0; 0 0];

normPredUNIh(1) = fitdist(predUNIh(1:hDP,2),'Normal');
normPredUNIh(2) = fitdist(predUNIh(hDP+1:3*hDP,2),'Normal');
normPredUNIh(3) = fitdist(predUNIh(3*hDP+1:4*hDP,2),'Normal');
[thresUNI(1,1), thresUNI(1,2)]=calcThres(normPredUNIh(1),normPredUNIh(2),normPredUNIh(3));

normPredUNIv(1) = fitdist(predUNIv(1:hDP,2),'Normal');
normPredUNIv(2) = fitdist(predUNIv(hDP+1:3*hDP,2),'Normal');
normPredUNIv(3) = fitdist(predUNIv(3*hDP+1:4*hDP,2),'Normal');
[thresUNI(2,1), thresUNI(2,2)]=calcThres(normPredUNIv(1),normPredUNIv(2),normPredUNIv(3));

normPredMC8h(1) = fitdist(predMC8h(1:hDP,2),'Normal');
normPredMC8h(2) = fitdist(predMC8h(hDP+1:3*hDP,2),'Normal');
normPredMC8h(3) = fitdist(predMC8h(3*hDP+1:4*hDP,2),'Normal');
[thresMC8(1,1), thresMC8(1,2)]=calcThres(normPredMC8h(1),normPredMC8h(2),normPredMC8h(3));

normPredMC8v(1) = fitdist(predMC8v(1:hDP,2),'Normal');
normPredMC8v(2) = fitdist(predMC8v(hDP+1:3*hDP,2),'Normal');
normPredMC8v(3) = fitdist(predMC8v(3*hDP+1:4*hDP,2),'Normal');
[thresMC8(2,1), thresMC8(2,2)]=calcThres(normPredMC8v(1),normPredMC8v(2),normPredMC8v(3));


% Separation of test set data 
llData = dataTestPart(find(cellfun( @(x) strcmp(x,'left'), {deventsTestPart.value})));
rrData = dataTestPart(find(cellfun( @(x) strcmp(x,'right'), {deventsTestPart.value})));
uuData = dataTestPart(find(cellfun( @(x) strcmp(x,'up'), {deventsTestPart.value})));
ddData = dataTestPart(find(cellfun( @(x) strcmp(x,'down'), {deventsTestPart.value})));
ulData = dataTestPart(find(cellfun( @(x) strcmp(x,'upleft'), {deventsTestPart.value})));
dlData = dataTestPart(find(cellfun( @(x) strcmp(x,'downleft'), {deventsTestPart.value})));
urData = dataTestPart(find(cellfun( @(x) strcmp(x,'upright'), {deventsTestPart.value})));
drData = dataTestPart(find(cellfun( @(x) strcmp(x,'downright'), {deventsTestPart.value})));

% Statistic Files
statsUNI = zeros(5,numel(dataTestPart));
statsMC8 = zeros(5,numel(dataTestPart));

% Direction Table
dirTable = [8, 1, 5; 4, 0, 2; 7, 3, 6];
indTable = [1 2; 2 3; 3 2; 2 1; 1 3; 3 3;3 1;1 1];

for i=1:8
    switch i
        case 1
            use_data=llData;
            nmb = 2;
        case 2
            use_data=rrData;
            nmb = 4;
        case 3
            use_data=uuData;
            nmb = 1;
        case 4
            use_data=ddData;
            nmb = 3;
        case 5
            use_data=ulData;
            nmb = 5;
        case 6
            use_data=dlData;
            nmb = 6;
        case 7
            use_data=urData;
            nmb = 8;
        case 8
            use_data=drData;
            nmb = 7;
    end
    for j=1:numel(use_data)
        [f_v,~,~]=buffer_apply_ersp_clsfr(use_data(j).buf,clsfrUNIv);
        [f_h,~,~]=buffer_apply_ersp_clsfr(use_data(j).buf,clsfrUNIh);
        [uniX, uniY] = getDir(f_h,f_v,thresUNI);
        uniDir = dirTable(uniY,uniX);
        statsUNI(:,(i-1)*numel(use_data)+j) = ...
            [nmb uniDir uniY==indTable(nmb,1) uniX==indTable(nmb,2) uniDir==nmb];
        [f_v,~,~]=buffer_apply_ersp_clsfr(use_data(j).buf,clsfrMC8v);
        [f_h,~,~]=buffer_apply_ersp_clsfr(use_data(j).buf,clsfrMC8h);
        [mk8X, mk8Y] = getDir(f_h,f_v,thresMC8);
        mk8Dir = dirTable(mk8Y, mk8X);
        statsMC8(:,(i-1)*numel(use_data)+j) = ...
            [nmb mk8Dir mk8Y==indTable(nmb,1) mk8X==indTable(nmb,2) mk8Dir==nmb];
    end
end

fprintf(['Universum Vertical Accuracy:\t\t' num2str(sum(statsUNI(3,:))...
    /numel(statsUNI(3,:))) '\n']);
fprintf(['Universum Horizontal Accuracy:\t\t' num2str(sum(statsUNI(4,:))...
    /numel(statsUNI(4,:))) '\n']);
fprintf(['Universum Total Accuracy:\t\t' num2str(sum(statsUNI(5,:))...
    /numel(statsUNI(5,:))) '\n']);
fprintf(['Multi-Class Vertical Accuracy:\t\t' num2str(sum(statsMC8(3,:))...
    /numel(statsMC8(3,:))) '\n']);
fprintf(['Multi-Class Horizontal Accuracy:\t' num2str(sum(statsMC8(4,:))...
    /numel(statsMC8(4,:))) '\n']);
fprintf(['Multi-Class Total Accuracy:\t\t' num2str(sum(statsMC8(5,:))...
    /numel(statsMC8(5,:))) '\n']);