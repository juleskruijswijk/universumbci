% Load a file from a subject
load('file.mat')
mkV = file_contents

% Since arrays are used for the different directions
% Loop over this array to make a list from it
for i = 1:3
    mu(i)=mkV(i).mu;
    sig(i)=mkV(i).sigma;
end
% Calculate the Bhatttacharyya distances for each direction oppossed.
b1=bhattacharyya(mu(1),sig(1),mu(2),sig(2));
b2= bhattacharyya(mu(1),sig(1),mu(3),sig(3));
b3= bhattacharyya(mu(2),sig(2),mu(3),sig(3));
b=b1+b2+b3;

% Now some plotting functions to make everything look nice
c=cell(3,1);
c{1}='b'
c{2}='g'
c{3}='r'
figure();
for i = 1:3
    ix = -5*sig(i):1e-5:5*sig(i); %covers more than 99% of the curve
    iy = pdf('normal', ix, mu(i), sig(i));
    plot(ix,iy,c{i});
    hold on;
end
legend('Right','Vertical','Left')
xlabel('Decision value')
ylabel('Relative number of examples')
annotation('textbox', [0.15,0.8,0.1,0.1],...
           'String', {['Right,Vertical = ' num2str(b1)], ['Right,Left = ' num2str(b2)], ['Vertical,Left = ' num2str(b3)] , ['Total = ' num2str(b)]})
       

