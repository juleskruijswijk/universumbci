function [threshold1, threshold2] = calcThres(norm1, norm2, norm3)
%   This function calculates the thresholds between the distributions,
%   calculated by applying the various classifiers on their training data.
%   The Thresholds are the intersection of the negative and zero class, and
%   the zero and positive class (unless the means of these three
%   distributions are not in descending order, then only the intersection
%   between the negative and positive class is calculated)
    if norm1.mu > norm2.mu & norm2.mu > norm3.mu
        x1 = norm1.mu:-0.1:norm2.mu;
        norm_pdf1 = normpdf(x1,norm1.mu,norm1.sigma);
        norm_pdf2 = normpdf(x1,norm2.mu,norm2.sigma);
        ind = find(min(abs(norm_pdf1-norm_pdf2)));
        threshold1 = x1(ind);
        x2 = norm2.mu:-0.1:norm3.mu;
        norm_pdf3 = normpdf(x2,norm2.mu,norm2.sigma);
        norm_pdf4 = normpdf(x2,norm3.mu,norm3.sigma);
        ind = find(min(abs(norm_pdf3-norm_pdf4)));
        threshold2 = x2(ind);
    elseif norm1.mu > norm3.mu
        x1 = norm1.mu:-0.1:norm3.mu;
        norm_pdf1 = normpdf(x1,norm1.mu,norm1.sigma);
        norm_pdf2 = normpdf(x1,norm3.mu,norm3.sigma);
        ind = find(min(abs(norm_pdf1-norm_pdf2)));
        threshold1 = x1(ind);
        threshold2 = x1(ind);
    end
end