run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% set the real-time-clock to use
initgetwTime;
initsleepSec;

% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

[cursor{1,1}, cursor{2,1}, cursor{2,2}] = imread('images/cursor2.png');
cursor{1,2} = [0.45 0.45 0.1 0.1];
[target{1,1}, target{2,1}, target{2,2}] = imread('images/target.png');
target{1,2} = [1.00 1.00 0.2 0.2];
target{1,3} = [0.65 0.65 0.2 0.2];
target{1,4} = [0.35 0.35 0.2 0.2];
target{1,5} = [0.75 0.15 0.2 0.2];
target{1,6} = [0.40 0.40 0.2 0.2];
target{1,7} = [0.65 0.65 0.2 0.2];
% Documenting test cell array
testDoc = cell(2400,3);

% GUI
pos_std = [.1 .1 .8 .8];
clf;
set(gcf,'color',[0 0 0],'toolbar','none','menubar','none',...
    'units','normalized','position',[0 0 1 1]);
ax1 = axes('visible','off','color',[0 0 0],'units','normalized','position',...
    pos_std);
ax2 = axes('visible','off','color',[0 0 0],'units','normalized','position',...
    pos_std);
ax3 = axes('visible','off','color',[0 0 0],'units','normalized','position',...
    pos_std);
axes(ax1);
%set(gca,'visible','off','color',[0 0 0],'units','normalized','position',...
%    pos_std);
tb_1=text(0.15,.9,'text','HorizontalAlignment','left','VerticalAlignment',...
    'top','FontUnits','normalized','fontsize',.04,'color',[1 1 1],...
    'visible','off');
tb_2=text(.5,.5,'text','HorizontalAlignment','center','VerticalAlignment',...
    'middle','FontUnits','normalized','fontsize',.10,'color',[1 1 1],...
    'visible','off');

% TESTING PHASE
% Display instructions
validSubj = false;
nSubj = 0;
while ~validSubj
    prompt = {'Enter Subject Number:'};
    dlg_title = 'Input';
    num_lines = 1;
    def = {'0'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    
    fileNameClsfrUNIv = ['data/subject_' num2str(answer{1,1}) '/universum/clsfr_vert' ];
    fileNameClsfrUNIh = ['data/subject_' num2str(answer{1,1}) '/universum/clsfr_hori' ];
    fileNameClsfrMC8v = ['data/subject_' num2str(answer{1,1}) '/multi_class_8/clsfr_vert' ];
    fileNameClsfrMC8h = ['data/subject_' num2str(answer{1,1}) '/multi_class_8/clsfr_hori' ];
    if exist([fileNameClsfrUNIv '.mat'], 'file') && exist([fileNameClsfrUNIh '.mat'], 'file') ...
            && exist([fileNameClsfrMC8v '.mat'], 'file') && exist([fileNameClsfrMC8h '.mat'], 'file')
        validSubj = true;
        nSubj = answer{1,1};
        sendEvent('univam.Subject', nSubj);
    else
        uiwait(msgbox('No / Not all classifier for this subject available!'));
    end
end
% Threshold
axes(ax1);
% for ii=1:numel(instr3)
%     set(tb_1,'string',instr3{ii},'visible','on');
%     drawnow; waitforbuttonpress;
% end
set(tb_1,'visible','off');
set(tb_2,'string','Blub','visible','on');
drawnow; waitforbuttonpress;
% Start Cursor Display
% Choose randomly first classifier
countBreak = 0;
whenUNI = 1;%randi(2);
clsfrNames = {'mk8' 'mk8'};
clsfrNames{whenUNI} = 'uni';
for i=1:2
    waitSigProc = true; state = [];
    while waitSigProc
        [devents,state]=buffer_newevents(buffhost,buffport,state,...
            {'univam.SigProcReady'},{},500);
        if (~isempty(devents))
            waitSigProc = false;
        end
    end
    % Send Event to start data allocation and choose respective
    % classifier
    if whenUNI==i
        fileNameThresUNI = ['data/subject_' num2str(nSubj) '/universum/thres' ];
        if exist([fileNameThresUNI '.mat'], 'file')
            thres = struct2array(load(fileNameThresUNI));
        else
            thres = [0 0; 0 0];
        end
        sendEvent('univam.CursorStart','uni');
    else
        fileNameThresMC8 = ['data/subject_' num2str(nSubj) '/multi_class_8/thres' ];
        if exist([fileNameThresMC8 '.mat'], 'file')
            thres = struct2array(load(fileNameThresMC8));
        else
            thres = [0 0; 0 0];
        end
        sendEvent('univam.CursorStart','mc8');
    end
    set(tb_2,'string','Push Button','visible','on');
    drawnow; waitforbuttonpress;
    set(tb_2,'string','Shift Calibration','visible','on');
    drawnow; waitforbuttonpress;
    set(tb_2,'visible','off');
    drawnow;
    sendEvent('univam.Thres.Hori', thres(1,:));
    sendEvent('univam.Thres.Vert', thres(2,:));
    % Shift Calculator
    shifting = true;
    shiftV = 0;
    shiftH = 0;
    shiftCounter = 0;
    tnm = 1;
    state = [];
    while (shifting)
        [devents,state]=buffer_newevents(buffhost,buffport,state,...
            {'univam.Pred'},{},0);
        %devents(1).value(1)=-1+rand*2;
        %devents(1).value(2)=-1+rand*2;
        %devents(2).value(1)=-1+rand*2;
        %devents(2).value(2)=-1+rand*2;
        for ei=1:numel(devents)
            % Calculate moves as -1, 0, 1 scalars
            moveV = (devents(ei).value(1)>thres(2,1)) - (devents(ei).value(1)<thres(2,2));
            moveH = (devents(ei).value(2)>thres(1,1)) - (devents(ei).value(2)<thres(1,2));
            
            shiftV = shiftV + devents(ei).value(1);
            shiftH = shiftH + devents(ei).value(2);
            shiftCounter = shiftCounter + 1;
            % Update GUI and state
            [cursor, target, tnm] = dispCursor([moveH,moveV],...
                cursor, target, tnm, ax2, ax3);
            
            % Reset target
            if (shiftCounter/50==1)
                cursor{1,2} = [0.45 0.45 0.1 0.1];
                target{1,2} = [cursor{1,2}(1)-0.05 cursor{1,2}(2)+0.25 0.2 0.2];
                shiftV = 0;
                shiftH = 0;
            elseif (shiftCounter/50==2)
                cursor{1,2} = [0.45 0.45 0.1 0.1];
                target{1,2} = [cursor{1,2}(1)+0.20 cursor{1,2}(2)-0.05 0.2 0.2];
            elseif (shiftCounter/50==3)
                cursor{1,2} = [0.45 0.45 0.1 0.1];
                target{1,2} = [cursor{1,2}(1)-0.05 cursor{1,2}(2)-0.40 0.2 0.2];
            elseif (shiftCounter/50==4)
                cursor{1,2} = [0.45 0.45 0.1 0.1];
                target{1,2} = [cursor{1,2}(1)-0.30 cursor{1,2}(2)-0.05 0.2 0.2];
            elseif (shiftCounter/50==5)
                shifting = false;
            end
        end
    end
    set(tb_2,'string','Cursor Control','visible','on');
    drawnow; waitforbuttonpress;
    set(tb_2,'visible','off');
    drawnow;
    shiftV = shiftV / (shiftCounter-50);
    shiftH = shiftH / (shiftCounter-50);
    sendEvent('univam.Shift', [shiftH shiftV]);
    cursor{1,2} = [0.45 0.45 0.1 0.1];
    target{1,2} = target{1,3};
    tnm = 2;
    counter = 1;
    testing = true;
    state=[];
    while (testing)
        [devents,state]=buffer_newevents(buffhost,buffport,state,...
            {'univam.Pred'},{},0);
        
        for ei=1:numel(devents)
            % Calculate moves as -1, 0, 1 scalars
            moveV = ((devents(ei).value(1)-shiftV)>thres(2,1)) ...
                - ((devents(ei).value(1)-shiftV)<thres(2,2));
            moveH = ((devents(ei).value(2)-shiftH)>thres(1,1)) ...
                - ((devents(ei).value(2)-shiftH)<thres(1,2));
            % Update GUI and state
            [cursor, target, tnm] = dispCursor([moveH,moveV],...
                cursor, target, tnm, ax2, ax3);
            % Document current state
            testDoc{countBreak + counter,1}=clsfrNames{i};
            testDoc{countBreak + counter,2}=cursor{1,2};
            testDoc{countBreak + counter,3}=target{1,2};
            counter = counter + 1;
        end
        % Switch to next / end test trial, when all targets were
        % visited or one trial has been
        if tnm == 5 || counter == 1200
            testing = false;
            countBreak = counter;
        end
    end
    
end

close;

