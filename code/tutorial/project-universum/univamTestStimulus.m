run ../../utilities/initPaths.m;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% set the real-time-clock to use
initgetwTime;
initsleepSec;

% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

% Time Database
stimDur = 2.0; % Duration of stimulus
clasDur = 2.0; % Duration of classification presentation
moveDur1 = 0.5; % Duration of movement
moveDur2 = 2.5;
timeOut = 5000;

%
nSeq = 10;
nSymbs = 8;

% Image Database
imgEmp = imread('images/test_phase_1/8arrow_base.png');
imgNoS = imread('images/test_phase_1/8arrow_fal_no.png');
imgDB{1,1} = imread('images/test_phase_1/8arrow_ind_up.png');
imgDB{1,2} = imread('images/test_phase_1/8arrow_ind_left.png');
imgDB{1,3} = imread('images/test_phase_1/8arrow_ind_down.png');
imgDB{1,4} = imread('images/test_phase_1/8arrow_ind_right.png');
imgDB{1,5} = imread('images/test_phase_1/8arrow_ind_upleft.png');
imgDB{1,6} = imread('images/test_phase_1/8arrow_ind_downleft.png');
imgDB{1,7} = imread('images/test_phase_1/8arrow_ind_downright.png');
imgDB{1,8} = imread('images/test_phase_1/8arrow_ind_upright.png');
imgDB{2,1} = imread('images/test_phase_1/8arrow_cor_up.png');
imgDB{2,2} = imread('images/test_phase_1/8arrow_cor_left.png');
imgDB{2,3} = imread('images/test_phase_1/8arrow_cor_down.png');
imgDB{2,4} = imread('images/test_phase_1/8arrow_cor_right.png');
imgDB{2,5} = imread('images/test_phase_1/8arrow_cor_upleft.png');
imgDB{2,6} = imread('images/test_phase_1/8arrow_cor_downleft.png');
imgDB{2,7} = imread('images/test_phase_1/8arrow_cor_downright.png');
imgDB{2,8} = imread('images/test_phase_1/8arrow_cor_upright.png');
imgDB{3,1} = imread('images/test_phase_1/8arrow_fal_up.png');
imgDB{3,2} = imread('images/test_phase_1/8arrow_fal_left.png');
imgDB{3,3} = imread('images/test_phase_1/8arrow_fal_down.png');
imgDB{3,4} = imread('images/test_phase_1/8arrow_fal_right.png');
imgDB{3,5} = imread('images/test_phase_1/8arrow_fal_upleft.png');
imgDB{3,6} = imread('images/test_phase_1/8arrow_fal_downleft.png');
imgDB{3,7} = imread('images/test_phase_1/8arrow_fal_downright.png');
imgDB{3,8} = imread('images/test_phase_1/8arrow_fal_upright.png');
imgPos = [0.25 0.25 0.5 0.5];

% Event Database
eventDB = {'up', 'left', 'down', 'right', 'upleft', 'downleft',...
    'downright', 'upright'};

% Direction Table
dirTable = [8, 1, 5; 4, 0, 2; 7, 3, 6];
indTable = [1 2; 2 3; 3 2; 2 1; 1 3; 3 3;3 1;1 1];


% Statistic File
statsUNI = zeros(5,nSeq*nSymbs); % 1: nmb of displayed stimulus 
                                 % 2: nmb of prediction
                                 % 3: vertical classification correct?
statsMC8 = zeros(5,nSeq*nSymbs); % 4: horizontal classification correct?
                                 % 5: overall classification correct?
                                 

% GUI
pos_std = [.1 .1 .8 .8];
clf;
set(gcf,'color',[0 0 0],'toolbar','none','menubar','none',...
    'units','normalized','position',[0 0 1 1]);
ax1 = axes('visible','off','color',[0 0 0],'units','normalized','position',...
    pos_std);
axes(ax1);
tb_1=text(0.15,.9,'text','HorizontalAlignment','left','VerticalAlignment',...
    'top','FontUnits','normalized','fontsize',.04,'color',[1 1 1],...
    'visible','off');
tb_2=text(.5,.5,'text','HorizontalAlignment','center','VerticalAlignment',...
    'middle','FontUnits','normalized','fontsize',.10,'color',[1 1 1],...
    'visible','off');

validSubj = false;
nSubj = 0;
while ~validSubj
    prompt = {'Enter Subject Number:'};
    dlg_title = 'Input';
    num_lines = 1;
    def = {'0'};
    answer = inputdlg(prompt,dlg_title,num_lines,def);
    
    fileNameClsfrUNIv = ['data/subject_' num2str(answer{1,1}) '/universum/clsfr_vert' ];
    fileNameClsfrUNIh = ['data/subject_' num2str(answer{1,1}) '/universum/clsfr_hori' ];
    fileNameClsfrMC8v = ['data/subject_' num2str(answer{1,1}) '/multi_class_8/clsfr_vert' ];
    fileNameClsfrMC8h = ['data/subject_' num2str(answer{1,1}) '/multi_class_8/clsfr_hori' ];
    if exist([fileNameClsfrUNIv '.mat'], 'file') && exist([fileNameClsfrUNIh '.mat'], 'file') ...
            && exist([fileNameClsfrMC8v '.mat'], 'file') && exist([fileNameClsfrMC8h '.mat'], 'file')
        validSubj = true;
        nSubj = answer{1,1};
        sendEvent('univam.Subject', nSubj);
    end
end

% Save files
fileNameStatsUNI = ['data/subject_' num2str(nSubj) '/universum/stats'];
fileNameStatsMC8 = ['data/subject_' num2str(nSubj) '/multi_class_8/stats'];

% Get Thresholds
thresUNI = [0 0; 0 0]; % upper row horizontal
thresMK8 = [0 0; 0 0];
fileNameThresUNI = ['data/subject_' num2str(nSubj) '/universum/thres'];
fileNameThresMC8 = ['data/subject_' num2str(nSubj) '/multi_class_8/thres'];
if exist([fileNameThresUNI '.mat'], 'file')
    load(fileNameThresUNI);
end
if exist([fileNameThresMC8 '.mat'], 'file')
    load(fileNameThresMC8);
end
    
% Display Instructions


% Start
himg = imshow(imgEmp);
set(gca,'position',imgPos);
set(himg,'visible','on');
drawnow; waitforbuttonpress;
for ei=1:nSeq
    tgtSeq=mkStimSeqRand(nSymbs,nSymbs);
    for ej=1:nSymbs
        % Find stimulus in the target sequence
        nmb = find(tgtSeq(:,ej));
        % Display Indicatin Stimulus
        himg = imshow(imgDB{1,nmb});
        drawnow; sleepSec(stimDur);
        % Movement Time
        himg = imshow(imgEmp);
        drawnow; sleepSec(moveDur1);
        % SendEvent
        sendEvent('univam.TestStimulus', eventDB{nmb}); sleepSec(moveDur2);
        % Wait for classification
        corPred = 0;
        nmbPred = 0;
        [devents,state]=buffer_newevents(buffhost,buffport,state,...
            {'univam.Pred.uni' 'univam.Pred.mc8'},{},timeOut);
        if (~isempty(devents))
            for ek=1:numel(devents)
                % Universum (presented)
                if ek==matchEvents(devents(ek), 'univam.Pred.uni', [])
                    [uniX, uniY] = getDir(devents(ek).value(2),devents(ek).value(1),thresUNI);
                    uniDir = dirTable(uniY,uniX);
                    statsUNI(:,((ei-1)*nSeq)+ej) = ...
                        [nmb uniDir uniY==indTable(nmb,1) uniX==indTable(nmb,2) uniDir==nmb];
                    corPred = 2 + (uniDir~=nmb);
                    nmbPred = uniDir;
                % MultiClass
                elseif ek==matchEvents(devents(ek), 'univam.Pred.mk8', [])
                    [mk8X, mk8Y] = getDir(devents(ek).value(2),devents(ek).value(1),thresMK8);
                    mk8Dir = dirTable(mk8Y, mk8X);
                    statsMC8(:,((ei-1)*nSeq)+ej) = ...
                        [nmb mk8Dir mk8Y==indTable(nmb,1) mk8X==indTable(nmb,2) mk8Dir==nmb];
                end
            end
        end
        % Display classification in right and wrong fashion
        if nmbPred == 0
            himg = imshow(imgNoS);
        else
            himg = imshow(imgDB{corPred,nmbPred});
        end
        drawnow; sleepSec(clasDur);
    end
end

save(fileNameStatsUNI, 'statsUNI');
save(fileNameStatsMC8, 'statsMC8');

sendEvent('univam.TestEnd', 'end');
close;