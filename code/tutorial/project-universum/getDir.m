function [indX, indY] = getDir(decValueX, decValueY, threshold)
%   Function returns the indices of a reference table.   
    if decValueX > threshold(1,1)
        indX = 1;
    elseif decValueX > threshold(1,2)
        indX = 2;
    else
        indX = 3;
    end
    
    if decValueY > threshold(2,1)
        indY = 1;
    elseif decValueY > threshold(2,2)
        indY = 2;
    else
        indY = 3;
    end
    
end