Task 1: instructed movement

In this task, you will be instructed to make left hand, right hand, 
tongue, and feet movements.

At the start of each trial, you will shortly see a picture on the screen 
indicating which movement to make. Next, a green cross will appear. 
Please start the movement as soon as the green cross disappears and 
keep moving until it appears again. There will be a brief pause before 
the next trial starts.

The different movements will now be explained.
