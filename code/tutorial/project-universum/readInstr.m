function instr = readInstr(nInstr, classString)

% classString = {'multiclass' , 'univam'}
    if strcmp(classString, 'univam1') | strcmp(classString, 'univam2') | strcmp(classString, 'univam3') ...
            | strcmp(classString, 'multiclass')
        for ins_i=1:nInstr
            temp_file = ['instructions/instr_', classString, '_', num2str(ins_i), '.txt'];
            if exist(temp_file, 'file')
                instr{ins_i} = dataread('file', temp_file, '%s', 'delimiter', '\n');
                instr{ins_i} = permute(instr{ins_i},[2 1]);
            end
        end
    end
end