run ../../utilities/initPaths;

buffhost='localhost';buffport=1972;
global ft_buff; ft_buff=struct('host',buffhost,'port',buffport);
% wait for the buffer to return valid header information
hdr=[];
while ( isempty(hdr) || ~isstruct(hdr) || (hdr.nchans==0) ) % wait for the buffer to contain valid data
  try 
    hdr=buffer('get_hdr',[],buffhost,buffport); 
  catch
    hdr=[];
    fprintf('Invalid header info... waiting.\n');
  end;
  pause(1);
end;

% set the real-time-clock to use
initgetwTime;
initsleepSec;


% ----------------------------------------------------------------------------
%    FILL IN YOUR CODE BELOW HERE
% ----------------------------------------------------------------------------

% useful functions
% get cap layout info
di = addPosInfo(hdr.channel_names,'1010'); % for a 1010 cap name set
ch_pos=cat(2,di.extra.pos2d); ch_names=di.vals; iseeg=[di.extra.iseeg]; % extract pos and channels names

trlen_samp = 50;
nSymbols = 2;
erp = zeros(sum(iseeg),trlen_samp,nSymbols);
nRight = 0;
nLeft = 0;

% make a template figure window with the channels in the positions as specified in the capfile
clf;
hdls=image3d(erp,1,'plotPos',ch_pos,'xlabel','ch','Xvals',ch_names,'zlabel','class','disptype','plot','ticklabs','sw');
drawnow;
state = [];
endExp = false;

while ~endExp
    [data,devents,state]=buffer_waitData([],[],state,'startSet',{'keyboard'},'trlen_samp',trlen_samp,'exitSet',{'data' 'stimulus.sequences' 'end'});
    
    for ei=1:numel(devents)
        event = devents(ei);
        dat = data(ei);
        
        if strcmp(event.value, 'l')
            erp(:,:,1)=(erp(:,:,1)*nLeft + dat.buf)/(nLeft + 1);
            nLeft = nLeft + 1;
        elseif strcmp(event.value, 'r')
            erp(:,:,2)=(erp(:,:,2)*nRight + dat.buf)/(nRight + 1);
            nRight = nRight+1;
        end
    end
    
    % update the info displayed above (without making new axes -- FASTER)
    hdls=image3d(erp,1,'handles',hdls,'xlabel','ch','Xvals',ch_names,'zlabel','class','disptype','plot','ticklabs','sw');
    drawnow;
end





% block until we've got new events which match 'startSet' and 'trlen_samp' data for each of these events
%  OR
%  until we've got an event matching 'exitSet'
% N.B. keep the state around to track what's been processed in subsequent calls
%[data,devents,state]=buffer_waitData([],[],state,'startSet',{'stimulus.epoch'},'trlen_samp',trlen_samp,'exitSet',{'data' 'stimulus.sequences' 'end'});